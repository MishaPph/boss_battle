## Boss Battle Game

A desktop version of 2D boss battle game made from scratch in C++/OpenGL.

**Controls**

| Key                         | Command              |
|-----------------------------|----------------------|
| <kbd>A</kbd>                | Walk left            |
| <kbd>D</kbd>                | Walk right           |
| <kbd>S + SPACe</kbd>        | Jump Down            |
| <kbd>F</kbd>                | Fire                 |
| <kbd>SPACE</kbd>            | Jump                 |

**Usage**

Everything needed is included in this repository, it also has Visual Studio project files.

**Dependencies**
- GLFW/GLAD
- GLM
- freetype
- enet

![til](./preview/first_version.gif)