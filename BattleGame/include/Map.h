#ifndef MAP_H
#define MAP_H

#include <string>
#include <memory>

struct ColorUC;

namespace beengine
{
	class Resources;
	class Atlas;
	class Quad;
}

namespace ecs
{
	class World;
}

class Map {
private:
	int width, height;
	ColorUC* data;
	std::shared_ptr<beengine::Atlas> atlas;
	std::unique_ptr<beengine::Quad> quad;
	std::shared_ptr<beengine::Resources> resources;
public:
	explicit Map(const std::shared_ptr<beengine::Resources>& resource);
	~Map();
	void load(const std::string& name);

	void setup(const std::shared_ptr<ecs::World>& world) const;
};

#endif // !MAP_H
