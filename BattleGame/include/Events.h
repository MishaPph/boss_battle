#pragma once
#ifndef EVENTS_H
#define EVENTS_H

#include "ecs/Event.h"

struct OnCollideEvent:public ecs::Event {
	ecs::EntityID a;
	ecs::EntityID b;
	int hitX;
	int hitY;

	OnCollideEvent(const ecs::EntityID first, const ecs::EntityID second, int x, int y) {
		a = first;
		b = second;
		hitX = x;
		hitY = y;
	}
};

struct HitEvent : public ecs::Event {
	ecs::EntityID target;
	HitEvent(ecs::EntityID t) {
		target = t;
	}
};

struct WeaponFireEvent : public ecs::Event {
	ecs::EntityID target;
	WeaponFireEvent(ecs::EntityID t) {
		target = t;
	}
};

struct BulletCreateEvent : public ecs::Event {
	
	short netId;
	float posX, posY;
	float directX, directY;

	BulletCreateEvent(): netId(0), posX(0), posY(0), directX(0), directY(0)
	{
	}

	BulletCreateEvent(short networkId, float x, float y, float directionX, float directionY) {
		netId = networkId;
		posX = x; posY = y; directX = directionX; directY = directionY;
	}
};

#endif