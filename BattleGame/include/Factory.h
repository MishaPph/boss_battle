#pragma once
#ifndef FACTORY_H
#define FACTORY_H

#include <memory>
#include <string>

namespace beengine
{
	class Resources;
}

namespace ecs {
	class World;
	typedef unsigned long long EntityID;
}

class Factory
{
	std::shared_ptr<beengine::Resources> resources;
public:
	Factory(const std::shared_ptr<beengine::Resources>& resource);
	~Factory();
	ecs::EntityID CreateHero(short networkId, int ownerId, bool controlledByUser, const std::shared_ptr<ecs::World>& world) const;
	void CreateBullet(int ownerId, const std::shared_ptr<ecs::World>& world, ecs::EntityID owner, 
		float x, float y, float directionX, float directionY) const;
	void CreateEnemyBullet(int ownerId, const std::shared_ptr<ecs::World>& world, ecs::EntityID owner,
		float x, float y, float directionX, float directionY) const;
	void CreateBoss(int ownerId, const std::shared_ptr<ecs::World>& world) const;
};

#endif