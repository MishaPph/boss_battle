#ifndef ANIMATION_H
#define ANIMATION_H

#include <string>

struct Animation {
	float time;
	int frame;
	int framePerSecond;
	std::string clip_name;
	bool loop = true;

	Animation(std::string name, int fps = 12) {
		clip_name = name;
		framePerSecond = fps;
		time = 0;
		frame = 0;
	}

	void play(const std::string& clip, bool l = true) {
		if (clip == clip_name)
			return;
		clip_name = clip;
		frame = 0;
		time = 0;
		loop = l;
	}
};

#endif