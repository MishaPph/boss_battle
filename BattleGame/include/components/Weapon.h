#pragma once
#ifndef WEAPON_H
#define WEAPON_H

#include <glm/ext/matrix_clip_space.hpp>
#include <glm/ext/matrix_transform.hpp>

struct Weapon {
	float cooldown;
	float timeShoot; 
	float bulletSpeed;

	glm::vec2 offset;
	int direction;

	bool readyToFire() const
	{
		return timeShoot <= 0;
	}
};

#endif