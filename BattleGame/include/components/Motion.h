#ifndef MOTION_H_
#define MOTION_H_

#include "glm/fwd.hpp"
#include <glm/glm.hpp>

#include <iostream>

struct Motion {
	glm::vec2 position;

	glm::vec2 velocity;

	glm::vec2 acceleration;
	glm::vec2 prevPosition;

	Motion(const glm::vec2& pos, const glm::vec2& vel): acceleration()
	{
		prevPosition = position = pos;
		velocity = vel;
	}

	friend std::ostream& operator<<(std::ostream& os, const Motion& motion)
	{
		os << '(' << motion.position.x << ',' << motion.position.y << ')';
		return os;
	}
};

#endif