#ifndef SPRITE_H_
#define SPRITE_H_

#include "beengine/Atlas.h"

#include <glm/glm.hpp>

class Sprite {
public:
	unsigned int VAO;
	std::shared_ptr<beengine::Atlas> atlas;
	std::string sprite_name;
	beengine::SpriteFrame frame;
	bool flipX = false;
	glm::vec3 color;

	glm::vec4 getTiling() const {
		const auto atlas_width = static_cast<float>(atlas->width);
		const auto atlas_height = static_cast<float>(atlas->height);
		
		const float height = frame.rect.size.y / atlas_height;
		const float width = frame.rect.size.x / atlas_width;
		return {width, height, frame.rect.position.x / atlas_width, 1.0f - height - frame.rect.position.y / atlas_height};
	}
};

#endif