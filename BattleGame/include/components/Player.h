#ifndef PLAYER_H_
#define PLAYER_H_

#include "MemoryStream.h"

struct Player {
	
	enum class LookVDirection : char
	{
		Top = 1,
		Down = -1,
		None = 0
	};

	enum class LookHDirection : char
	{
		Right = 1,
		Left = -1,
		None = 0
	};
	
	enum State : char
	{
		Idle = 0,
		Sit = 1,
		Jump = 2,
		Fall = 3,
		Run = 4,
		Death = 5
	};
	
	State state;
	LookVDirection vDirection;
	LookHDirection hDirection;
	bool jump() const { return jumpDirect != 0;}
	char jumpDirect;
	short flyTime = 0;
	bool fly() const { return flyTime != 0; }
	short jumpTime = 0;
	short resurrectTime = 0;

public:
	std::ostream& print(std::ostream& os) {
		os << '(' << (int)state << ',' << (int)vDirection << ',' << (int)hDirection << ')' << jumpDirect << ',' << flyTime;
		return os;
	}

	void write(OutMemoryStream& out)
	{
		out.write((char*)&state, sizeof(State));
		out.write((char*)&vDirection, sizeof(LookVDirection));
		out.write((char*)&hDirection, sizeof(LookHDirection));
		out.write((char*)&jumpDirect, sizeof(char));
		out.write((char*)&flyTime, sizeof(short));
		out.write((char*)&jumpTime, sizeof(short));

		//in.write((char*)&y, sizeof(y));
	}

	void read(InMemoryStream& in) {
		in.read((char*)&state, sizeof(State));
		in.read((char*)&vDirection, sizeof(LookVDirection));
		in.read((char*)&hDirection, sizeof(LookHDirection));
		in.read((char*)&jumpDirect, sizeof(char));
		in.read((char*)&flyTime, sizeof(short));
		in.read((char*)&jumpTime, sizeof(short));
	}
};

#endif