#pragma once
#ifndef COLLIDER_H
#define COLLIDER_H

#include "beengine/Rect.h"
#include "Physics.h"

struct Collider {
	glm::vec2 offset;
	glm::vec2 size;
	CollisionFilterGroups layer;
};
#endif