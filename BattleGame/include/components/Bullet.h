#pragma once
#ifndef BULLET_H
#define BULLET_H

struct Bullet {
	ecs::EntityID ownerId;

	int damage;

	float lifeTime;

	float age;
};

#endif