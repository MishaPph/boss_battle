#pragma once
#ifndef PLAYER_INPUT_H_
#define PLAYER_INPUT_H_

struct PlayerInput {
	int horizontal;
	int vertical;
	bool fire;
	bool jump;
};
#endif