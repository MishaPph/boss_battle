#ifndef BLINK_H
#define BLINK_H

#include <glm/glm.hpp>

struct Blink {
	float duration;
	float rate;
	glm::vec3 base_color;
	float life;

};
#endif