#pragma once
#ifndef _GAME_H
#define _GAME_H

#include "ecs/World.h"

#include <chrono>

#include "Factory.h"
#include "beengine/Window.h"
#include "beengine/Camera.h"

class Network;

enum GameMode
{
	Offline = 0,
	Server = 1,
};

class Game
{
private:
	float maxFrameTime;

	double accumulator;
	double currentTime;

	void updateSystems(float dt, float alpha);
	void fixedUpdate();

	std::shared_ptr<ecs::World> pWorld;
	std::shared_ptr<beengine::Window> window;
	std::shared_ptr<Network> pNetwork;
	std::shared_ptr<ecs::EntityManager> entityMngr;
	std::shared_ptr<beengine::Camera> pCamera;

	std::chrono::time_point<std::chrono::steady_clock> startTime;

	int simulationFrame;
	bool showColliders;

	GameMode gameMode;
	
	void drawWorld();
	void drawMenu();
	bool IsServerKeyDown() const;
	bool IsOfflineKeyDown() const;
	
	void createSystems(const std::shared_ptr<Factory>& factory);
	void createNetworkSystems(bool server, const std::shared_ptr<Factory>& factory);
	double timeSinceStart() const;
public:
	Game();
	~Game();

	void init();
	void run();
	void close() const;
};

#endif // !_GAME_H