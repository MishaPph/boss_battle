#ifndef _MEMORY_STREAM_H_
#define _MEMORY_STREAM_H_
#pragma once

#include <sstream>
#include <ostream>
#include <streambuf>


struct OMemBuf : std::streambuf
{
	OMemBuf(char* base, size_t size)
	{
		this->setp(base, base + size);
	}
};

struct OutMemoryStream : virtual OMemBuf, std::ostream
{
	OutMemoryStream(char* base, size_t size) :
		OMemBuf(base, size),
		std::ostream(static_cast<std::streambuf*>(this))
	{
	}
};

struct IMemBuf : std::streambuf
{
	IMemBuf(const char* base, size_t size)
	{
		char* p(const_cast<char*>(base));
		this->setg(p, p, p + size);
	}
};

struct InMemoryStream : virtual IMemBuf, std::istream
{
	InMemoryStream(const char* mem, size_t size) :
		IMemBuf(mem, size),
		std::istream(static_cast<std::streambuf*>(this))
	{
	}
};

#endif