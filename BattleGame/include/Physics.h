#pragma once
#ifndef PHYSICS_H
#define PHYSICS_H

enum class CollisionFilterGroups {
	None = 0,
	Ground = 1,
	Player = 2,
	Enemy = 4
};

class Physics {

private:
	static int collision_matrix[8];
public:

	static bool collide(CollisionFilterGroups layer1, CollisionFilterGroups layer2);
	static void registerGroup(CollisionFilterGroups layer1, CollisionFilterGroups layer2);
	static void printMatrix();
};

#endif