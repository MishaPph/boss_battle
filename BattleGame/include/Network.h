#pragma once

#include <memory>
#include <functional>

#ifndef NETWORK_H
#define NETWORK_H

struct _ENetHost;
typedef _ENetHost ENetHost;

struct _ENetPeer;
typedef _ENetPeer ENetPeer;

class INetworkListener
{
public:
	virtual ~INetworkListener() = default;
	virtual void serverStarted() = 0;
	virtual void clientConnected(int clientId) = 0;
	virtual void clientDisconnected(int clientId) = 0;
	virtual void messageReceived(char *data, int size) = 0;
};

enum class ConnectionState
{
	None = 0,
	Connecting = 1,
	Connected = 2
};

class Network
{
private:
	ENetHost* server;
	ENetHost* client;

	ENetPeer* clientPeer;
	INetworkListener* pListener;
	
	std::function<void(int)> on_client_connected;
	int client_id = 0;
	int waitingTimeToConnectToServer = 0;
	
	bool createServer();
	bool createClient();
	void sendClientId(ENetPeer* peer);
	void updateClient();
	void updateServer();
public:

	ConnectionState state;

	void init(INetworkListener* listener);

	int connect(bool server, const std::function<void(int)>& connected);

	void update();
	void disconnect();
	void clear();

	bool isServer() const { return server != nullptr; }
	bool isClient() const { return client != nullptr; }

	int clientId() const { return client_id; }

	void sendPacketReliable(const void *data, int size) const;
	void sendPacketUnreliable(const void *data, int size, int ID) const;

	bool IsServer;
};

#endif