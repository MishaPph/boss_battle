#pragma once
#ifndef PLAYER_SYSTEM_H
#define PLAYER_SYSTEM_H

#include <cmath>

#include "ecs/System.h"

const double pi = std::acos(-1.0);

struct Weapon;
struct HitEvent;
struct Player;
struct OnCollideEvent;
struct Sprite;

class PlayerSystem : public ecs::System {

public:
	virtual ~PlayerSystem() = default;
	void init() override;

	void onCollide(const OnCollideEvent& collide);

	void fixedUpdate() override;

	void update(float dt, float alpha) override;

private:
	void onHit(const HitEvent& evnt);
	void updateWeapon(Weapon& weapon, const Player& player, const Sprite& sprite, bool fire);
	void updateColliderSize(ecs::EntityID enId, const Player* player);
};

#endif