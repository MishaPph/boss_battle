#ifndef PLAYER_INPUT_SYSTEM_H
#define PLAYER_INPUT_SYSTEM_H

#include "Components/PlayerInput.h"
#include "Components/Sprite.h"
#include "Components/Motion.h"

#include "ecs/SceneView.h"

class PlayerInputSystem : public ecs::System {

public:
	virtual ~PlayerInputSystem() = default;

	explicit PlayerInputSystem(GLFWwindow* window) :pWindow(window) {
	}

	void update(float dt, float alpha) override {

		for (const auto ent : ecs::SceneView<PlayerInput, Sprite, Motion>(parentWorld->GetEntityManager()))
		{
			auto input = parentWorld->Get<PlayerInput>(ent);
			input->horizontal = 0;
			input->vertical = 0;

			if (glfwGetKey(pWindow, GLFW_KEY_W) == GLFW_PRESS) {
				input->vertical = 1;
			}
			if (glfwGetKey(pWindow, GLFW_KEY_S) == GLFW_PRESS) {
				input->vertical = -1;
			}

			if (glfwGetKey(pWindow, GLFW_KEY_A) == GLFW_PRESS) {
				input->horizontal = -1;
			}
			if (glfwGetKey(pWindow, GLFW_KEY_D) == GLFW_PRESS) {
				input->horizontal = 1;
			}

			input->fire = glfwGetKey(pWindow, GLFW_KEY_F) == GLFW_PRESS;

			input->jump = glfwGetKey(pWindow, GLFW_KEY_SPACE) == GLFW_PRESS && glfwGetKey(pWindow, GLFW_KEY_SPACE) != GLFW_REPEAT;
		}
	}
private:
	GLFWwindow* pWindow;
};

#endif