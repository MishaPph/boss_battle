#ifndef ENEMY_SYSTEM_H
#define ENEMY_SYSTEM_H

#include "ecs/System.h"

class EnemySystem final : public ecs::System {
public:
	virtual ~EnemySystem() = default;
	void update(float dt, float alpha) override;
	void fixedUpdate() override;
};

#endif