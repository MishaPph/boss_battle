#pragma once

#ifndef ANIMATION_SYSTEM_H
#define ANIMATION_SYSTEM_H

#include "ecs/System.h"

struct Animation;
class Sprite;

class AnimationSystem final: public ecs::System {
public:
	virtual ~AnimationSystem() = default;
	void init() override {}

	void update(float dt, float alpha) override;
private:
	static void updateAnimation(Animation& animation, Sprite& sprite);
};

#endif