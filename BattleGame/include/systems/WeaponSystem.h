#ifndef WEAPON_SYSTEM_H
#define WEAPON_SYSTEM_H
#pragma once

#include "ecs/System.h"
#include "Events.h"

class Factory;

class WeaponSystem final :public ecs::System {
	std::shared_ptr<Factory> factory;
public:
	WeaponSystem(const std::shared_ptr<Factory>& factory);
	virtual ~WeaponSystem() = default;
	
	void init() override;
	void update(float dt, float alpha) override;
	void fire(const WeaponFireEvent& evnt);
};
#endif