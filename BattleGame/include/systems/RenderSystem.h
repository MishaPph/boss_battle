
#ifndef RENDER_SYSTEM_H
#define RENDER_SYSTEM_H

#include "ecs/System.h"

class Sprite;
class Position;

namespace beengine {
	class Camera;
	class Shader;
}

class RenderSystem : public ecs::System {
private:
	std::shared_ptr<beengine::Camera> camera;
public:
	virtual ~RenderSystem() = default;

	RenderSystem(const std::shared_ptr<beengine::Camera> m_camera);

	void init() override;

	void update(float dt, float alpha) override;

private:
	std::shared_ptr<beengine::Shader> shader;

	void Draw(const Sprite* sprite, const Position* position) const;
};

#endif