#ifndef NETWORK_SYSTEM_H
#define NETWORK_SYSTEM_H
#pragma once

#include "ecs/System.h"
#include "Events.h"
#include "Network.h"


class Factory;

enum NetworkOperation : char 
{
	SendHero = 1,
	SyncHero = 2,
	SendCreateBullet = 3
};

class NetworkSystem final: public ecs::System, INetworkListener, std::enable_shared_from_this<NetworkSystem> {
	std::shared_ptr<Network> pNetwork;
	std::shared_ptr<Factory> pFactory;
	bool isServer;
	void spawnHero(class InMemoryStream& stream);
	void syncHero(InMemoryStream& stream);
	void createBullet(InMemoryStream& stream);
	void onBulletCreated(const BulletCreateEvent& evnt);
public:
	
	NetworkSystem(bool isServer, const std::shared_ptr<Network>& network, const std::shared_ptr<Factory>& factory);
	~NetworkSystem() override;
	
	void init() override;

	void fixedUpdate() override;
	void serverStarted() override;
	void clientConnected(int clientId) override;
	void clientDisconnected(int clientId) override;
	void messageReceived(char *data, int size) override;

    typedef std::map<int, short> OpMap;

	static OpMap maps;

	static short newId(int clientId);
};
#endif