#pragma once
#ifndef BLINK_SYSTEM_H
#define BLINK_SYSTEM_H

#include "ecs/System.h"

class BlinkSystem final: public ecs::System {

public:
	virtual ~BlinkSystem() = default;
	void update(float dt, float alpha) override;
};


#endif