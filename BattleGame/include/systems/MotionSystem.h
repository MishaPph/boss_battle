#ifndef MOTION_SYSTEM_H
#define MOTION_SYSTEM_H

#include "ecs/System.h"

#include "GameTime.h"

class MotionSystem : public ecs::System {

public:
	MotionSystem() {}

	static float lerp(const float a, const float b, const float lerp)
	{
		return a + lerp * (b - a);
	}

	void update(float dt, float alpha) override;
	void fixedUpdate() override;
};

#endif