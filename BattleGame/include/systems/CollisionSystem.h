#pragma once
#ifndef COLLISION_SYSTEM_H
#define COLLISION_SYSTEM_H

#include "ecs/System.h"
#include "beengine/Rect.h"
#include <glm/glm.hpp>

struct Motion;

class CollisionSystem final: public ecs::System {
	beengine::Rect screen_rect;
public:
	explicit CollisionSystem(const beengine::Rect& screenRect): screen_rect(screenRect)
	{
	}

	virtual ~CollisionSystem() = default;
	
	void fixedUpdate() override;

private:

	void updatePlayerCollisions();
	bool hitTarget(const glm::vec2& position, const glm::vec2& size, const ecs::EntityID targetEntity);
	void checkEnemyHit(ecs::EntityID ent, const class Sprite* sprite, const Motion* bulletP);
	void checkPlayerHit(ecs::EntityID ent, const Sprite*  sprite, const Motion* bulletP);
	static bool clampByScreenBorder(const beengine::Rect& rect, Motion* pos1);

	bool checkCollision(const glm::vec2& position, const glm::vec2& pivot, const glm::vec2& size, const beengine::Rect& vol2);
	static bool intersects(const glm::vec2& posA, const glm::vec2& sizeA, const glm::vec2& posB, const glm::vec2& sizeB);
	static void resolveCollision(Motion* pos1, const glm::vec2& size, const beengine::Rect& collisionRect, glm::lowp_i8vec2& outHitNormal);
};
#endif