#pragma once
#ifndef ANIMATOR_SYSTEM_H
#define ANIMATOR_SYSTEM_H

#include "ecs/System.h"

struct Animator;
struct Player;
struct Animation;

class AnimatorSystem final: public ecs::System {
public:
	virtual ~AnimatorSystem() = default;
	void init() override {}

	void update(float dt, float alpha) override;
private:
	void updateAnimator(Animator* animator, const Player* player, Animation* animation);
};

#endif