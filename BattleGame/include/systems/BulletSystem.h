#pragma once
#ifndef BULLET_SYSTEM_H
#define BULLET_SYSTEM_H

#include "ecs/System.h"

class BulletSystem final: public ecs::System {

public:
	virtual ~BulletSystem() = default;
	void fixedUpdate() override;
};
#endif