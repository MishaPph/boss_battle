#pragma once
#ifndef DRAW_COLLIDER_SYSTEM_H
#define DRAW_COLLIDER_SYSTEM_H

#include <glm/vec2.hpp>
#include <glm/vec4.hpp>

#include "ecs/System.h"

struct Collider;
class Position;

namespace beengine {
	class Shader;
	class Quad;
	class Camera;
}

class DrawColliderSystem final : public ecs::System {

public:
	explicit DrawColliderSystem(const std::shared_ptr<beengine::Camera>& m_camera);
	virtual ~DrawColliderSystem();

	void update(float dt, float alpha) override;

private:
	std::unique_ptr<beengine::Quad> quad;
	
	std::shared_ptr<beengine::Shader> shader;
	std::shared_ptr<beengine::Camera> camera;

	void Draw(const Collider& collider, const glm::vec2& position) const;
	void Draw(const glm::vec2& position, const glm::vec2& size, const glm::vec4& color) const;
};

#endif