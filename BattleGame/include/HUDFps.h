#ifndef _HUDFPS_H
#define _HUDFPS_H

class HUDFps {
private:
	float updateInterval = 0.5F;
	float accum = 0; // FPS accumulated over the interval
	int   frames = 0; // Frames drawn over the interval
	float timeleft; // Left time for current interval
public:
	float fps = 60;
	HUDFps() {
		timeleft = updateInterval;
	}

	void update(float dt) {
		timeleft -= dt;
		accum += 1.0f / dt;
		++frames;

		if (timeleft <= 0.0)
		{
			fps = accum / frames;
			timeleft = updateInterval;
			accum = 0.0F;
			frames = 0;
		}
	}
};

#endif
