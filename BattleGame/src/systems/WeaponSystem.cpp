#include "Components/Weapon.h"
#include "Components/Player.h"
#include "Components/Position.h"
#include "Components/Sprite.h"
#include "Components/NetworkSync.h"

#include "systems/WeaponSystem.h"
#include "ecs/System.h"
#include "ecs/SceneView.h"
#include "Events.h"

#include <cmath>

#include "Factory.h"

WeaponSystem::WeaponSystem(const std::shared_ptr<Factory>& factory) : factory(factory)
{
}

void WeaponSystem::init() {
	parentWorld->GetEventBus()->subscribe<WeaponSystem, WeaponFireEvent>(this, &WeaponSystem::fire);
}

void WeaponSystem::update(float dt, float alpha) {
	for (const auto ent : ecs::SceneView<Weapon>(parentWorld->GetEntityManager()))
	{
		Get<Weapon>(ent)->timeShoot -= dt;
	}
}

void WeaponSystem::fire(const WeaponFireEvent& evnt) {

	const auto weapon = Get<Weapon>(evnt.target);
	if (weapon->timeShoot > 0) {
		return;
	}
	
	const auto position = Get<Position>(evnt.target);
	const auto angle = 3.14f / 180.0f * weapon->direction;
	const float x = cos(angle);
	const float y = sin(angle);

	const auto positionX = position->x + weapon->offset.x;
	const auto positionY = position->y + weapon->offset.y;

	const auto flySpeedX = x * weapon->bulletSpeed;
	const auto flySpeedY = y * weapon->bulletSpeed;

	if(parentWorld->Has<NetworkSync>(evnt.target)) {
		const auto networkSync = Get<NetworkSync>(evnt.target);
		parentWorld->GetEventBus()->publish<BulletCreateEvent>(BulletCreateEvent(networkSync->uniqId, positionX, positionY, flySpeedX, flySpeedY));
	}

	if (parentWorld->Has<Player>(evnt.target)) {
		for (int i = -5; i < 5; i++)
		{
			for (int j = -5; j < 5; j++)
			{
				factory->CreateBullet(0, parentWorld, evnt.target, positionX + i*1, positionY+j*1, flySpeedX + i * 40, flySpeedY + j * 40);
			}
		}
	}
	else {
		factory->CreateEnemyBullet(0, parentWorld, evnt.target, positionX, positionY, flySpeedX, flySpeedY);
	}

	weapon->timeShoot = weapon->cooldown;
}