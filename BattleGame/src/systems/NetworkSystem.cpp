#include "systems/NetworkSystem.h"
#include "ecs/System.h"
#include "ecs/SceneView.h"

#include "Network.h"
#include "Events.h"
#include "Components/NetworkSync.h"
#include "MemoryStream.h"

#include "Components/Player.h"
#include "Components/Motion.h"

#include "Factory.h"

NetworkSystem::OpMap NetworkSystem::maps = { { 0, 0 } }; 

short NetworkSystem::newId(const int clientId)
{
	maps[clientId]++;
	return maps[clientId];
}

NetworkSystem::NetworkSystem(const bool isServer, const std::shared_ptr<Network>& network,
							 const std::shared_ptr<Factory>& factory) : pNetwork(network), pFactory(factory), isServer(isServer)
{
}

NetworkSystem::~NetworkSystem()
{
}

void NetworkSystem::init() {
	pNetwork->init(this);
	pNetwork->connect(isServer, std::bind(&NetworkSystem::clientConnected, this, true));

	parentWorld->GetEventBus()->subscribe<NetworkSystem, BulletCreateEvent>(this, &NetworkSystem::onBulletCreated);
};

void NetworkSystem::serverStarted() {

}

void NetworkSystem::clientDisconnected(int clientId) {

}

void NetworkSystem::clientConnected(const int clientId) {
	std::cout << "clientConnected " << clientId << ", is server " << isServer << "\n";
	
	if (!isServer)
		return;
	
	pFactory->CreateHero(newId(clientId), clientId, false, parentWorld);

	for (ecs::EntityID ent : ecs::SceneView<NetworkSync>(parentWorld->GetEntityManager()))
	{
		const auto networkSync = Get<NetworkSync>(ent);
		constexpr int package_size = sizeof(unsigned long long) + sizeof(int) + sizeof(short) + sizeof(float)*2;
		char buff[package_size];
		if (parentWorld->Has<Player>(ent)) {
			std::cout << "send connected " << ent << "\n";
			OutMemoryStream mem(buff, package_size);

			char operationId = static_cast<char>(NetworkOperation::SendHero);
			mem.write(&operationId, sizeof(char));

			mem.write(reinterpret_cast<char*>(&ent), sizeof(unsigned long long));
			mem.write((char *)&networkSync->ownerId, sizeof(int));
			mem.write((char *)&networkSync->uniqId, sizeof(short));

			auto motion = Get<Motion>(ent);

			mem.write((char *)&motion->position.x, sizeof(float));
			mem.write((char *)&motion->position.y, sizeof(float));

			pNetwork->sendPacketReliable(buff, package_size);
		}
	}
	pNetwork->update();
}

void NetworkSystem::messageReceived(char *data, int size) {
	InMemoryStream mem(data, size);
	char operationId = 1;
	mem.read(&operationId, sizeof(char));
	
	switch (operationId)
	{
	case NetworkOperation::SendHero:
		spawnHero(mem);
		break;
	case NetworkOperation::SyncHero:
		syncHero(mem);
		break;
	case NetworkOperation::SendCreateBullet:
		createBullet(mem);
		break;
	default:
		break;
	}
}

void NetworkSystem::spawnHero(InMemoryStream& stream) {

	ecs::EntityID id;
	stream.read((char *)&id, sizeof(unsigned long long));

	int ownerId;
	stream.read((char *)&ownerId, sizeof(int));

	short uniqId;
	stream.read((char *)&uniqId, sizeof(short));

	float x; float y;
	stream.read((char *)&x, sizeof(float));
	stream.read((char *)&y, sizeof(float));

	std::cout << "spawnHero " << id << " controlled " <<  (pNetwork->clientId() == ownerId) << "\n";

	const auto entiyty = pFactory->CreateHero(uniqId, ownerId, pNetwork->clientId() == ownerId, parentWorld);
	const auto motion = Get<Motion>(entiyty);
	motion->position.x = x;
	motion->position.y = y;
}

void NetworkSystem::syncHero(InMemoryStream& stream) {

	short id;
	stream.read((char *)&id, sizeof(short));
	int ownerId;
	stream.read((char *)&ownerId, sizeof(int));

	for (const ecs::EntityID ent : ecs::SceneView<NetworkSync>(parentWorld->GetEntityManager()))
	{
		auto networkSync = Get<NetworkSync>(ent);
		if(networkSync->uniqId == id && !networkSync->owner)
		{
			const auto motion = Get<Motion>(ent);
			if(motion == nullptr) {
				std::cout << "fail to get motion " << ent << "\n";
				for (const auto netEntityId : ecs::SceneView<NetworkSync>(parentWorld->GetEntityManager()))
				{
					std::cout << "exist " << netEntityId << "\n";
				}
				return;
			}
			
			stream.read((char *)&motion->position.x, sizeof(float));
			stream.read((char *)&motion->position.y, sizeof(float));

			auto player = Get<Player>(ent);
			player->read(stream);
			break;
		}
	}

	//player->print(std::cout) << std::endl;
}

void NetworkSystem::createBullet(InMemoryStream& stream)
{
	BulletCreateEvent evnt;
	stream.read((char *)&evnt, sizeof(BulletCreateEvent));

	std::cout << "createBullet " << evnt.netId << "\n";

	for (ecs::EntityID ent : ecs::SceneView<NetworkSync>(parentWorld->GetEntityManager()))
	{
		auto networkSync = Get<NetworkSync>(ent);
		if(networkSync->uniqId == evnt.netId)
		{
			if (parentWorld->Has<Player>(ent)) {
				pFactory->CreateBullet(networkSync->ownerId, parentWorld, ent, evnt.posX, evnt.posY, evnt.directX, evnt.directY);
			} else {
				pFactory->CreateEnemyBullet(networkSync->ownerId, parentWorld, ent, evnt.posX, evnt.posY, evnt.directX, evnt.directY);
			}
			return;
		}
	}
	std::cout << "fail create, not found  " << evnt.netId << "\n";
}

void NetworkSystem::onBulletCreated(const BulletCreateEvent& evnt)
{
	constexpr int size = sizeof(char) + sizeof(BulletCreateEvent);

	char buff[size];
	OutMemoryStream mem(buff, size);

	char operationId = static_cast<char>(NetworkOperation::SendCreateBullet);
	mem.write(&operationId, sizeof(char));

	mem.write((char *)&evnt, sizeof(BulletCreateEvent));

	pNetwork->sendPacketReliable(buff, size);
}

void NetworkSystem::fixedUpdate() {
	for (const auto ent : ecs::SceneView<NetworkSync>(parentWorld->GetEntityManager()))
	{
		const auto networkSync = Get<NetworkSync>(ent);
		if (!networkSync->owner) {
			continue;
		}

		char buff[128];
		OutMemoryStream mem(buff, 128);

		char operationId = static_cast<char>(NetworkOperation::SyncHero);
		mem.write(&operationId, sizeof(char));

		//std::cout << ent << " sended " << std::endl;
		mem.write((char *)&networkSync->uniqId, sizeof(short));
		mem.write((char *)&networkSync->ownerId, sizeof(int));

		const auto motion = Get<Motion>(ent);

		mem.write((char *)&motion->position.x, sizeof(float));
		mem.write((char *)&motion->position.y, sizeof(float));

		const auto player = Get<Player>(ent);
		player->write(mem);

		pNetwork->sendPacketReliable(buff, 128);
	}
	pNetwork->update();
}
