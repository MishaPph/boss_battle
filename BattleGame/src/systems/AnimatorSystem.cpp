
#include "systems/AnimatorSystem.h"
#include "ecs/SceneView.h"

#include "Components/Player.h"
#include "Components/Animator.h"
#include "Components/Animation.h"
#include "Components/Sprite.h"

void AnimatorSystem::update(float dt, float alpha) {
	for (auto ent : ecs::SceneView<Player, Animator, Animation, Sprite>(parentWorld->GetEntityManager()))
	{
		auto player = Get<Player>(ent);
		auto animator = Get<Animator>(ent);
		auto animation = Get<Animation>(ent);
		auto sprite = Get<Sprite>(ent); 
		if (player->state == Player::State::Run) {
			sprite->flipX = player->hDirection == Player::LookHDirection::Left;
		}
		updateAnimator(animator, player, animation);
	}
}

void AnimatorSystem::updateAnimator(Animator* animator, const Player* player, Animation* animation) {
	if (player->state == Player::State::Death) {
		animation->play("death", false);
		return;
	}
	if (player->jump())
	{
		animation->play("jump");
		return;
	}
	if (player->flyTime > 2) {
		animation->play("fall");
		return;
	}

	if (player->state == Player::State::Run) {

		if (player->vDirection == Player::LookVDirection::Down) {
			animation->play("run_bottom");
		}
		else if (player->vDirection == Player::LookVDirection::Top) {
			animation->play("run_top");
		}
		else {
			animation->play("run");
		}
	}
	else {
		if (player->vDirection == Player::LookVDirection::Top) {
			animation->play("top");
		}
		else if (player->state == Player::State::Sit) {
			animation->play("sit");
		}
		else {
			animation->play("idle");
		}
	}
}