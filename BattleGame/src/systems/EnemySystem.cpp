#include "Systems/EnemySystem.h"

#include "ecs/SceneView.h"

#include "components/Weapon.h"
#include "components/Position.h"
#include "components/Enemy.h"
#include "components/Player.h"

#include <cmath>

#include "Events.h"

void EnemySystem::update(float dt, float alpha) {

}

void EnemySystem::fixedUpdate() {
	for (const auto ent : ecs::SceneView<Weapon, Position, Enemy>(parentWorld->GetEntityManager()))
	{
		const auto weapon = Get<Weapon>(ent);
		if (!weapon->readyToFire())
			continue;
		
		bool found = false;
		const auto enemyPos = Get<Position>(ent);
		for (const auto ent2 : ecs::SceneView<Position, Player>(parentWorld->GetEntityManager()))
		{
			const auto pos = Get<Position>(ent2);
			const auto player = Get<Player>(ent2);
			if (player->state != Player::State::Run)
				continue;
			found = true;
			const auto norm = glm::normalize(glm::vec2(pos->x - enemyPos->x, pos->y - enemyPos->y));
			weapon->direction = atan2(norm.y, norm.x) * 180.0f / 3.14159265359f;
		}
		
		if(found)
			parentWorld->GetEventBus()->publish<WeaponFireEvent>(WeaponFireEvent(ent));
	}
}