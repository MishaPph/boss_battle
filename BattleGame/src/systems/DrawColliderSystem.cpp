#include "systems/DrawColliderSystem.h"

#include "ecs/SceneView.h"

#include <glm/ext/matrix_clip_space.hpp>
#include <glm/ext/matrix_transform.hpp>

#include "Components/Collider.h"
#include "Components/Position.h"

#include "beengine/Resources.h"
#include "beengine/Shader.h"
#include "beengine/Camera.h"
#include "beengine/Quad.h"
#include "components/Bullet.h"
#include "components/Motion.h"
#include "components/Sprite.h"

DrawColliderSystem::DrawColliderSystem(const std::shared_ptr<beengine::Camera>& m_camera)
{
	camera = m_camera;
	quad = std::unique_ptr<beengine::Quad>();
	shader = std::make_shared<beengine::Shader>("debugCollider");
}

DrawColliderSystem::~DrawColliderSystem()
{
}

void DrawColliderSystem::update(float dt, float alpha) {
	shader->use();
	shader->setMat4("projection", camera->matrix);
	shader->setMat4("view", glm::mat4(1.0f));

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	for (const auto ent : ecs::SceneView<Collider, Position>(parentWorld->GetEntityManager()))
	{
		const auto& collider = GetRef<Collider>(ent);
		const auto& position = GetRef<Position>(ent);
		Draw(collider, glm::vec2(position.x, position.y));
	}

	for (const auto ent : ecs::SceneView<Bullet, Motion, Sprite>(parentWorld->GetEntityManager()))
	{
		const auto motion = Get<Motion>(ent);
		const auto sprite = Get<Sprite>(ent);
		Draw(motion->position, sprite->frame.rect.size, glm::vec4(0.9, 0.3f, 0.3f, 0.3f));
	}
	
	/*for (auto ent : ecs::SceneView<Sprite, Position>(*parentWorld->pEntitymngr))
	{
		auto collider = Get<Sprite>(ent);
		auto position = Get<Position>(ent);
		Draw(Collider{ glm::vec2(0,0), collider->frame.rect.size}, position);
	};
	*/
	glDisable(GL_BLEND);
};

void DrawColliderSystem::Draw(const Collider& collider, const glm::vec2& position) const
{
	Draw(position + collider.offset, collider.size, glm::vec4(0.3, 0.9f, 0.3f, 0.3f));
}

void DrawColliderSystem::Draw(const glm::vec2& position, const glm::vec2& size, const glm::vec4& color) const
{
	auto model = glm::mat4(1.0f);
	const glm::vec2 pivot(0.5f, 0.5f);
	model = translate(model, glm::vec3(position.x - pivot.x * size.x,
		position.y- pivot.y * size.y, 0.0f));
	model = scale(model, glm::vec3(size.x,size.y, 1.0f));

	shader->setMat4("model", model);
	shader->setVec4("color", color);

	glBindVertexArray(quad->VAO);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glBindVertexArray(0);
}
