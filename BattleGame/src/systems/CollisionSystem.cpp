#include "systems/CollisionSystem.h"

#include "ecs/World.h"
#include "ecs/SceneView.h"

#include "components/Player.h"
#include "components/Bullet.h"
#include "components/Collider.h"
#include "components/Sprite.h"
#include "components/Position.h"
#include "components/Enemy.h"
#include "components/Blink.h"
#include "components/Motion.h"

#include "Events.h"

#include <iostream>

#include <glm/glm.hpp>
#include "beengine/Rect.h"

void CollisionSystem::updatePlayerCollisions()
{
	const auto& pivot = glm::vec2(0.5f, 0.5f);

	for (const auto ent : ecs::SceneView<Player, Motion, Collider>(parentWorld->GetEntityManager()))
	{
		const auto player = Get<Player>(ent);
		if (player->jumpTime > 0)
			continue;

		const auto playerCollider = Get<Collider>(ent);

		const auto motion = Get<Motion>(ent);

		if (clampByScreenBorder(screen_rect, motion)) {
			parentWorld->GetEventBus()->publish<OnCollideEvent>(OnCollideEvent(ent, ent, 0, 1));
		}

		bool collided = false;
		for (const auto ent2 : ecs::SceneView<Collider, Position>(parentWorld->GetEntityManager()))
		{
			if (ent2 == ent)
				continue;

			const auto collider = Get<Collider>(ent2);

			if (!Physics::collide(playerCollider->layer, collider->layer)) 
				continue;

			const auto position = Get<Position>(ent2);

			int k = 0;
			glm::lowp_i8vec2 hitNormal = {0,0};
			
			beengine::Rect rect2{ glm::vec2(position->x + collider->offset.x - collider->size.x / 2.0f,
			                                position->y + collider->offset.y - collider->size.y / 2.0f), collider->size };

			auto size = playerCollider->size;
			while (checkCollision(motion->position, pivot, size, rect2) && k++ < 2) {
				resolveCollision(motion, size, rect2, hitNormal);
				collided = true;
			}
			if (collided) {
				if (checkCollision(motion->position, pivot, size, rect2)) {
					std::cout << "fail to resolve" << "\n";
				}
				parentWorld->GetEventBus()->publish<OnCollideEvent>(OnCollideEvent(ent, ent2, hitNormal.x, hitNormal.y));
				break;
			}
		}
	}
}

void CollisionSystem::fixedUpdate() {
	
	updatePlayerCollisions();
	
	for (const auto ent : ecs::SceneView<Bullet, Motion, Sprite>(parentWorld->GetEntityManager()))
	{
		const auto sprite = Get<Sprite>(ent);
		const auto bulletP = Get<Motion>(ent);
		const auto bullet = Get<Bullet>(ent);
		const auto player = Get<Player>(bullet->ownerId);
		if (player != nullptr)
		{
			checkEnemyHit(ent, sprite, bulletP);
			continue;
		}

		const bool enemyBullet = Get<Enemy>(bullet->ownerId);
		if (enemyBullet)
		{
			checkPlayerHit(ent, sprite, bulletP);
		}
	}
}

bool CollisionSystem::hitTarget(const glm::vec2& position, const glm::vec2& size, const ecs::EntityID target)
{
	const auto pivot = glm::vec2(0.5f, 0.5f);
	const auto positionC = Get<Position>(target);
	const auto collider = Get<Collider>(target);
	const auto pos = glm::vec2(positionC->x, positionC->y);
	const beengine::Rect rr { glm::vec2(
		pos + collider->offset - collider->size * pivot),
		collider->size };

	const beengine::Rect rect { position - size * pivot, size };

	//std::cout << rect << " with " << rr << " = "<< rect.intersects(rr) << '\n';
	return rect.intersects(rr);
}

void CollisionSystem::checkEnemyHit(const ecs::EntityID ent, const Sprite* sprite, const Motion* bulletP)
{
	for (const auto targetEntity : ecs::SceneView<Enemy, Collider, Position>(parentWorld->GetEntityManager()))
	{
		if (!hitTarget(bulletP->position, sprite->frame.rect.size, targetEntity)) 
			continue;
		
		const auto enemy = Get<Enemy>(targetEntity);
		parentWorld->AddComponent<Blink>(targetEntity, Blink{ 0.5f, 10.0f });
		parentWorld->AddComponent<Blink>(enemy->partId, Blink{ 0.5f, 10.0f });
		parentWorld->GetEventBus()->publish<HitEvent>(HitEvent(targetEntity));
		parentWorld->GetEntityManager()->DestroyEntity(ent);
		return;
	}
}

void CollisionSystem::checkPlayerHit(const ecs::EntityID ent, const Sprite* sprite, const Motion* bulletP)
{
	for (const auto target : ecs::SceneView<Player, Collider, Position, Sprite>(parentWorld->GetEntityManager()))
	{
		if (!hitTarget(bulletP->position, sprite->frame.rect.size, target))
			continue;
				
		const auto playerSprite = Get<Sprite>(target);
		parentWorld->AddComponent<Blink>(target, Blink{ 0.5f, 10.0f, playerSprite->color,});
		std::cout << "hit player" << "\n";
		parentWorld->GetEventBus()->publish<HitEvent>(HitEvent(target));
		parentWorld->GetEntityManager()->DestroyEntity(ent);
		break;
	}
}

bool CollisionSystem::clampByScreenBorder(const beengine::Rect& rect, Motion* pos1) {
	pos1->position.x = glm::clamp(pos1->position.x, rect.position.x, rect.position.x + rect.size.x);
	
	if (pos1->position.y < rect.position.y)
	{
		pos1->position.y = rect.position.y;
		return true;
	}
	
	pos1->position.y = glm::clamp(pos1->position.y, rect.position.y, rect.position.y + rect.size.y);
	return false;
}

bool CollisionSystem::checkCollision(const glm::vec2& position, const glm::vec2& pivot, const glm::vec2& size, const beengine::Rect& vol2)
{
	const beengine::Rect rect { position - size * pivot, size };
	return rect.intersects(vol2);
}

bool CollisionSystem::intersects(const glm::vec2& posA, const glm::vec2& sizeA, const glm::vec2& posB, const glm::vec2& sizeB)
{
	const beengine::Rect rectA{ posA - sizeA * 0.5f, sizeA };
	const beengine::Rect rectB{ posB - sizeB * 0.5f, sizeB };
	return rectA.intersects(rectB);
}

void CollisionSystem::resolveCollision(Motion* pos1, const glm::vec2& size, const beengine::Rect& collisionRect, glm::lowp_i8vec2& outHitNormal)
{
	const auto pos = pos1->position - size * 0.5f;
	const float px1 = collisionRect.position.x + collisionRect.size.x - pos.x;
	const float px2 = pos.x + size.x - collisionRect.position.x;
	const float x = std::min(px1, px2);

	const float py1 = collisionRect.position.y + collisionRect.size.y - pos.y;
	const float py2 = pos.y + size.y - collisionRect.position.y;
	const float y = std::min(py1, py2);
	outHitNormal.x = 0;
	outHitNormal.y = 0;
	
	if (x < y * 0.5f) {
		if (px1 < px2) {
			pos1->position.x += px1;
			outHitNormal.x = 1;
		}
		else {
			pos1->position.x -= px2;
			outHitNormal.x = -1;
		}
	}
	else {
		if (py1 < py2) {
			pos1->position.y += py1;
			outHitNormal.y = 1;
		}
		else {
			pos1->position.y -= py2;
			outHitNormal.y = -1;
		}
	}
}