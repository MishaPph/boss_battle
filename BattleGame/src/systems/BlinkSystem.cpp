#include "systems/BlinkSystem.h"

#include "Components/Blink.h"
#include "Components/Sprite.h"

#include "ecs/System.h"
#include "ecs/SceneView.h"


void BlinkSystem::update(float dt, float alpha) {
	for (const auto ent : ecs::SceneView<Sprite, Blink>(parentWorld->GetEntityManager()))
	{
		const auto sprite = Get<Sprite>(ent);
		const auto blink = Get<Blink>(ent);

		blink->life += dt;
		if (blink->duration <= blink->life) {
			//std::cout << "remove blink" << ent << '\n';
			parentWorld->RemoveComponent<Blink>(ent);
			sprite->color = blink->base_color;
		}
		else {
			const auto color = std::sin(blink->life * blink->rate * 3.14f)*0.5f;
			sprite->color = glm::vec3(color, color, color);
		}
	}
};