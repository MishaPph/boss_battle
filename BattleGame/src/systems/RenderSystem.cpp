#include "systems/RenderSystem.h"
#include "Components/Position.h"
#include "Components/Sprite.h"

#include "ecs/SceneView.h"
#include "beengine/Camera.h"
#include "beengine/Core.h"
#include "beengine/Shader.h"

#include <glm/ext/matrix_transform.hpp>

RenderSystem::RenderSystem(const std::shared_ptr<beengine::Camera> m_camera) {
	camera = m_camera;
}

void RenderSystem::init() {
	shader = std::make_shared<beengine::Shader>("unlit");
}

void RenderSystem::update(float dt, float alpha) {
	shader->use();
	shader->setMat4("projection", camera->matrix);
	shader->setMat4("view", glm::mat4(1.0f));

	for (const auto ent : ecs::SceneView<Sprite, Position>(parentWorld->GetEntityManager()))
	{
		const auto sprite = Get<Sprite>(ent);
		const auto position = Get<Position>(ent);
		Draw(sprite, position);
	}
}

void RenderSystem::Draw(const Sprite* sprite, const Position* position) const
{
	auto model = glm::mat4(1.0f);
	glm::vec2 v(sprite->frame.rect.size);
	const int flip_value = sprite->flipX ? -1 : 1;

	model = glm::translate(model, glm::vec3(position->x - sprite->frame.pivot.x * v.x * flip_value, position->y - sprite->frame.pivot.y*v.y, 0.0f));
	shader->setVec4("tiling", sprite->getTiling());
	model = glm::scale(model, glm::vec3(v.x * flip_value, v.y, 1.0f));
	shader->setMat4("model", model);
	shader->setVec3("color", sprite->color);

	shader->setInt("texture1", 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, sprite->atlas->textureId);

	glBindVertexArray(sprite->VAO);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glBindVertexArray(0);
}
