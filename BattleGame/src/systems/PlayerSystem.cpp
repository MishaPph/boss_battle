#include "systems/PlayerSystem.h"

#include "Components/Player.h"
#include "Components/PlayerInput.h"
#include "Components/Sprite.h"
#include "Components/Position.h"
#include "Components/Motion.h"
#include "Components/Weapon.h"
#include "Components/Collider.h"
#include "Components/NetworkSync.h"

#include "ecs/SceneView.h"
#include "ecs/System.h"
#include "ecs/World.h"

#include "Events.h"
#include "Network.h"

void PlayerSystem::init() {
	parentWorld->GetEventBus()->subscribe<PlayerSystem, OnCollideEvent>(this, &PlayerSystem::onCollide);
	parentWorld->GetEventBus()->subscribe<PlayerSystem, HitEvent>(this, &PlayerSystem::onHit);
}

void PlayerSystem::onHit(const HitEvent& evnt) {
	const auto player = Get<Player>(evnt.target);
	if (player != nullptr) {
		//std::cout << "call back player" << std::endl;
		player->state = Player::State::Death;
		player->resurrectTime = 150;
		updateColliderSize(evnt.target, player);
	}
}

void PlayerSystem::onCollide(const OnCollideEvent& collide) {

	if (!parentWorld->Has<Player>(collide.a)) {
		return;
	}
	const auto player = Get<Player>(collide.a);
	const auto motion = Get<Motion>(collide.a);

	if (collide.hitY == 1) {
		motion->velocity.y = std::max(0.0f, motion->velocity.y);
		player->jumpDirect = 0;
		player->flyTime = 0;
	}
	else if (collide.hitY == -1) {
		motion->velocity.y = std::min(0.0f, motion->velocity.y);
	}
	else if (collide.hitX == 1) {
		motion->velocity.x = std::max(0.0f, motion->velocity.x);
	}
	else if (collide.hitX == -1) {
		motion->velocity.x = std::min(0.0f, motion->velocity.x);
	}
}

void PlayerSystem::fixedUpdate() {
	for (const auto ent : ecs::SceneView<PlayerInput, Player>(parentWorld->GetEntityManager()))
	{
		auto& player = GetRef<Player>(ent);
		player.flyTime++;
		player.jumpTime--;
	}
}

void PlayerSystem::update(float dt, float alpha) {

	for (const auto ent : ecs::SceneView<PlayerInput, Player, Sprite, Motion>(parentWorld->GetEntityManager()))
	{
		const auto input = Get<PlayerInput>(ent);
		const auto player = Get<Player>(ent);
		const auto motion = Get<Motion>(ent);

		const auto prevState = player->state;
		if (player->state == Player::State::Death) {
			motion->velocity.x = 0;
			player->resurrectTime--;
			if (player->resurrectTime == 0) {
				player->state = Player::State::Idle;
				Get<Motion>(ent)->position = glm::vec2(0, 0);
				Get<Position>(ent)->x = 0;
				Get<Position>(ent)->y = 0;
				updateColliderSize(ent, player);
			}
			continue;
		}

		const auto sprite = Get<Sprite>(ent);
		const auto weapon = Get<Weapon>(ent);

		const auto lastHDirection = player->hDirection;

		player->vDirection = static_cast<Player::LookVDirection>(input->vertical);
		player->hDirection = static_cast<Player::LookHDirection>(input->horizontal);

		if (player->state != Player::State::Jump || player->flyTime == 0) {
			if (player->hDirection != Player::LookHDirection::None) {
				player->state = Player::State::Run;
			}
			else if (player->vDirection == Player::LookVDirection::Down) {
				player->state = Player::State::Sit;
			}
			else {
				player->state = Player::State::Idle;
			}
		} 

		if(player->state != Player::State::Run && player->state != Player::State::Jump)
			player->hDirection = lastHDirection;

		motion->velocity.x = input->horizontal * 200.0f;

		if (!player->fly() && input->jump) {
			if (player->vDirection == Player::LookVDirection::Down) {
				motion->velocity.y -= 50;
				player->jumpDirect = -1;
			}
			else {
				motion->velocity.y += 150;
				player->jumpDirect = 1;
			}
			player->state = Player::State::Jump;
			player->jumpTime = 16;
			player->flyTime = 1;
		}

		updateWeapon(*weapon, *player, *sprite, input->fire);
		if (input->fire) {
			const auto networkSync = Get<NetworkSync>(ent);
			if(networkSync->owner)
				parentWorld->GetEventBus()->publish<WeaponFireEvent>(WeaponFireEvent(ent));
		}
		if (prevState != player->state) {
			updateColliderSize(ent, player);
		}
	}
}

void PlayerSystem::updateWeapon(Weapon& weapon, const Player& player, const Sprite& sprite, const bool fire) {
	if (player.state == Player::State::Run || player.state == Player::State::Idle || player.state == Player::State::Sit) {
		weapon.offset.x = !sprite.flipX * (sprite.frame.rect.size.x) - sprite.frame.rect.size.x / 2;
	} if (player.vDirection == Player::LookVDirection::Top) {
		weapon.offset.x =  -3 + !sprite.flipX * 5;
	}

	if (player.vDirection == Player::LookVDirection::Top) {
		weapon.offset.y = sprite.frame.rect.size.y/2 - 1;
	}
	else if (player.vDirection == Player::LookVDirection::Down) {
		weapon.offset.y = 0;
	}
	else {
		weapon.offset.y = 6;
	}

	weapon.direction = 0;
	if (fire) {
		int y = static_cast<int>(player.vDirection);
		int x = static_cast<int>(player.hDirection);
		if (player.state == Player::State::Sit) {
			y = 0;
		}
		if (player.state == Player::State::Idle && player.vDirection == Player::LookVDirection::Top) {
			x = 0;
		}
		if (player.hDirection != Player::LookHDirection::None || player.vDirection != Player::LookVDirection::None)
			weapon.direction = atan2(y, x) * 180.0f / pi;
	}
}

void PlayerSystem::updateColliderSize(const ecs::EntityID enId, const Player* player) {
	const auto collider = Get<Collider>(enId);
	if (player->state == Player::State::Sit) {
		collider->size = glm::vec2(32, 32);
	} else if (player->state == Player::State::Jump) {
		collider->size = glm::vec2(24, 24);
	}
	else if (player->state == Player::State::Death) {
		collider->size = glm::vec2(32, 6);
	}
	else{
		collider->size = glm::vec2(32, 40);
	}
}