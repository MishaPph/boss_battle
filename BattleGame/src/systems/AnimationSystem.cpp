#include "systems/AnimationSystem.h"
#include "ecs/SceneView.h"

#include "Components/Sprite.h"
#include "Components/Animation.h"

void AnimationSystem::update(const float dt, float alpha)  {
	for (const auto ent : ecs::SceneView<Sprite, Animation>(parentWorld->GetEntityManager()))
	{
		Sprite& sprite = *parentWorld->Get<Sprite>(ent);
		Animation& animation = *parentWorld->Get<Animation>(ent);
		animation.time += dt;
		updateAnimation(animation, sprite);
	}
}

void AnimationSystem::updateAnimation(Animation& animation, Sprite& sprite) {
	const auto frameTime = 1.0f / static_cast<float>(animation.framePerSecond);
	const auto animFrames = sprite.atlas->getAnimation(animation.clip_name);

	if (animFrames.empty())
		return;
	
	const auto countFrame = animFrames.size();

	bool dirty = false;
	while (animation.time >= frameTime) {
		animation.time -= frameTime;
		animation.frame++;

		if (animation.frame >= static_cast<int>(countFrame)) {
			animation.frame = (animation.frame - 1) * !animation.loop;
		}
		sprite.sprite_name = animFrames[animation.frame];
		sprite.frame = sprite.atlas->getSpriteFrame(sprite.sprite_name);
	}
}