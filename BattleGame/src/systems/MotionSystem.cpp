#include "systems/MotionSystem.h"

#include "ecs/SceneView.h"
#include "components/Motion.h"
#include "components/Position.h"
#include "components/Player.h"
#include "components/NetworkSync.h"

void MotionSystem::update(float dt, float alpha) {
	for (const auto ent : ecs::SceneView<Position, Motion>(parentWorld->GetEntityManager()))
	{
		const auto motion = Get<Motion>(ent);
		const auto position = Get<Position>(ent);
		position->x = lerp(motion->prevPosition.x, motion->position.x, alpha);
		position->y = lerp(motion->prevPosition.y, motion->position.y, alpha);
	}
}

void MotionSystem::fixedUpdate() {
	const auto dt = GameTime::fixedDeltaTime;

	for (const auto ent : ecs::SceneView<Motion>(parentWorld->GetEntityManager()))
	{
		const auto motion = Get<Motion>(ent);

		if (parentWorld->Has<Player>(ent)) {
			if(parentWorld->Has<NetworkSync>(ent) && Get<NetworkSync>(ent)->owner)
				motion->velocity.y -= 90 * dt;
		}

		motion->prevPosition = motion->position;
		motion->position.x += motion->velocity.x*dt;
		motion->position.y += motion->velocity.y*dt;
	}
}