#include "systems/BulletSystem.h"
#include "ecs/SceneView.h"

#include "components/Bullet.h"

#include "GameTime.h"

void BulletSystem::fixedUpdate()
{
	for (const auto ent : ecs::SceneView<Bullet>(parentWorld->GetEntityManager()))
	{
		const auto bullet = Get<Bullet>(ent);
		bullet->age += GameTime::fixedDeltaTime;
		if (bullet->age > bullet->lifeTime)
		{
			parentWorld->GetEntityManager()->DestroyEntity(ent);
		}
	}
}