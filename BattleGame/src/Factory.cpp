#include "Factory.h"

#include "Components/Sprite.h"
#include "Components/Position.h"
#include "Components/Motion.h"
#include "Components/Animation.h"
#include "Components/Player.h"
#include "Components/PlayerInput.h"
#include "Components/Animator.h"
#include "Components/Weapon.h"
#include "Components/Bullet.h"
#include "Components/Collider.h"
#include "Components/Enemy.h"
#include "Components/NetworkSync.h"

#include "beengine/Resources.h"
#include "beengine/Quad.h"
#include "beengine/GeometryObjectFactory.h"

#include "ecs/EntityManager.h"
#include "ecs/World.h"

#include <utility>
#include <iostream>


Factory::Factory(const std::shared_ptr<beengine::Resources>& resource) : resources(resource)
{
}

Factory::~Factory() = default;

ecs::EntityID Factory::CreateHero(short networkId, int ownerId, bool controlledByUser, const std::shared_ptr<ecs::World>& pWorld) const
{
	const auto heroEntityId = pWorld->GetEntityManager()->NewEntity();
	std::cout << "created " << heroEntityId << "\n";

	const auto atlas = resources->loadAtlas("hero");

	atlas->registerAnimation("fall", { "fall_1" });
	atlas->registerAnimation("idle", { "idle_1" });
	atlas->registerAnimation("top", { "top_1" });
	atlas->registerAnimation("sit", { "sit_1" });

	atlas->getSpriteFrame("top_1").pivot.y = 0.36f;
	atlas->getSpriteFrame("sit_1").pivot.y = 0.66f;

	pWorld->AddComponent<Position>(heroEntityId, Position{ 0,0 });
	pWorld->AddComponent<Player>(heroEntityId, Player{  });

	auto sprite = Sprite{beengine::GeometryObjectFactory::GetQuad().VAO, atlas, "idle_1", atlas->getSpriteFrame("idle_1") };

	if (controlledByUser) {
		pWorld->AddComponent<PlayerInput>(heroEntityId, PlayerInput{ });
		pWorld->AddComponent<Collider>(heroEntityId, Collider{ glm::vec2(0,0), glm::vec2(32, 40), CollisionFilterGroups::Player });
		sprite.color = glm::vec3(0.0f, 0.0f, 0.2f);
	} else{
		sprite.color = glm::vec3(0.2f, 0.0f, 0.f);
	}

	pWorld->AddComponent<Sprite>(heroEntityId, std::move(sprite));

	pWorld->AddComponent<Motion>(heroEntityId, Motion(glm::vec2(0,0), glm::vec2(0, 0)));
	pWorld->AddComponent<Animation>(heroEntityId, std::move(Animation("run")));
	pWorld->AddComponent<Animator>(heroEntityId, Animator());
	pWorld->AddComponent<Weapon>(heroEntityId, Weapon{0.5f, 0.0f, 400.0f});
	pWorld->AddComponent<NetworkSync>(heroEntityId, NetworkSync{ ownerId, controlledByUser,  networkId});

	return heroEntityId;
}

void Factory::CreateBullet(const int ownerId, const std::shared_ptr<ecs::World>& pWorld, const ecs::EntityID owner, const float x, float y,
                           const float directionX, float directionY) const
{
	//std::cout << "CreateBullet " << x << ":" << y << " " << directionX << ":" << directionY << "\n";

	const auto atlas = resources->loadAtlas("hero");
	const ecs::EntityID a = pWorld->GetEntityManager()->NewEntity();
	pWorld->AddComponent<Sprite>(a, std::move(Sprite{ beengine::GeometryObjectFactory::GetQuad().VAO, atlas, "bullet", atlas->getSpriteFrame("bullet") }));
	pWorld->AddComponent<Position>(a, Position{ x, y });
	pWorld->AddComponent<Motion>(a, Motion (glm::vec2(x, y), glm::vec2(directionX, directionY)));
	pWorld->AddComponent<Bullet>(a, Bullet { owner, 10, 5.0f });
	pWorld->AddComponent<NetworkSync>(a, NetworkSync { ownerId, false });
}

void Factory::CreateEnemyBullet(const int ownerId, const std::shared_ptr<ecs::World>& pWorld, const ecs::EntityID owner, float x, float y,
                                float directionX, float directionY) const
{
	std::cout << "CreateEnemyBullet " << x << ":" << y << "\n";

	const auto atlas = resources->loadAtlas("boss");

	ecs::EntityID a = pWorld->GetEntityManager()->NewEntity();
	pWorld->AddComponent<Sprite>(a, std::move(Sprite{ beengine::GeometryObjectFactory::GetQuad().VAO, atlas, "bullet_1", atlas->getSpriteFrame("bullet_1") }));
	pWorld->AddComponent<Position>(a, Position{ x, y });
	pWorld->AddComponent<Motion>(a, Motion(glm::vec2(x, y), glm::vec2(directionX, directionY)));
	pWorld->AddComponent<Bullet>(a, Bullet{ owner, 10, 1.0f });
	pWorld->AddComponent<NetworkSync>(a, NetworkSync{ ownerId });
}

void Factory::CreateBoss(int ownerId, const std::shared_ptr<ecs::World>& pWorld) const
{
	const auto atlas = resources->loadAtlas("boss");

	const ecs::EntityID mouth = pWorld->GetEntityManager()->NewEntity();
	pWorld->AddComponent<Sprite>(mouth, Sprite{ beengine::GeometryObjectFactory::GetQuad().VAO, atlas, "mouth_1", atlas->getSpriteFrame("mouth_1") });

	pWorld->AddComponent<Position>(mouth, Position{ 0, -46 });
	const ecs::EntityID head = pWorld->GetEntityManager()->NewEntity();
	pWorld->AddComponent<Sprite>(head, std::move(Sprite{ beengine::GeometryObjectFactory::GetQuad().VAO, atlas, "Head", atlas->getSpriteFrame("Head") }));
	pWorld->AddComponent<Position>(head, Position{ 0, 30 });
	pWorld->AddComponent<Enemy>(head, Enemy{ mouth });
	pWorld->AddComponent<Collider>(head, Collider {glm::vec2(0,0), glm::vec2(50, 50), CollisionFilterGroups::Enemy });
	pWorld->AddComponent<Weapon>(head, Weapon{ 1.5f, 0.0f, 200.0f});
	pWorld->AddComponent<NetworkSync>(head, NetworkSync{ ownerId });
}
