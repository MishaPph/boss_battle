#include "Physics.h"
#include <iostream>

int Physics::collision_matrix[8] = { 0,0,0,0,0,0,0,0 };

bool Physics::collide(CollisionFilterGroups layer1, CollisionFilterGroups layer2) {
	return (collision_matrix[static_cast<int>(layer1)] & static_cast<int>(layer2)) == static_cast<int>(layer2);
}

void Physics::registerGroup(CollisionFilterGroups layer1, CollisionFilterGroups layer2) {
	auto l1 = static_cast<int>(layer1);
	auto l2 = static_cast<int>(layer2);
	collision_matrix[l1] |= l2;
	collision_matrix[l2] |= l1;
}

void Physics::printMatrix() {
	for (size_t i = 0; i < 8; i++)
	{
		std::cout << " " << collision_matrix[i];
	}
	std::cout << "\n";
}