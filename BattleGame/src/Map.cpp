#include "Map.h"

#include <string>
#include <iostream>

#include "beengine/Resources.h"
#include "beengine/Quad.h"

#include "ecs/EntityManager.h"
#include "ecs/World.h"

#include "Components/Sprite.h"
#include "Components/Position.h"
#include "Components/Collider.h"
#include "Components/Motion.h"

using namespace ecs;
using namespace  beengine;

struct ColorUC {
	unsigned char r;
	unsigned char g;
	unsigned char b;
};

Map::Map(const std::shared_ptr<Resources>& resource) :
	width(0),
	height(0),
	data(nullptr),
	resources(resource)
{
}

Map::~Map()
{
	resources->unloadRawImage(data);
}

void Map::load(const std::string& name) {
	data = reinterpret_cast<ColorUC*>(resources->loadRawImage("Maps/" + name, &width, &height));
	if (!data) {
		std::cout <<"Map " << name << " not found" << "\n";
	}
	std::cout << "Map " << name << " size:" << width << " " << height << " loaded" << "\n";
	atlas = resources->loadAtlas("location");
	quad = std::make_unique<Quad>();
}

void Map::setup(const std::shared_ptr<World>& pWorld) const
{
	int k = 0;
	for (auto i = 0; i < width; i++)
	{
		for (auto j = 0; j < height; j++)
		{
			const auto r = data[k];
			k++;
			const auto pos = glm::vec2((j - height / 2.0f + 0.5f) * 32, (i - width / 2.0f + 0.5f) * 32);

			if (r.r > 0 || r.b > 0 || r.g > 0) {
				const auto& entity_id = pWorld->GetEntityManager()->NewEntity();
				std::string sprite_name;
				if (r.b == 255) {
					sprite_name = "water";
				} else if (r.b == 100) {
					sprite_name = "deepwater";
				}
				else if (r.g == 255) {
					sprite_name = "forest";
				}
				else if (r.r == 255) {
					sprite_name = "road";
					const auto frame = atlas->getSpriteFrame(sprite_name);
					pWorld->AddComponent<Collider>(entity_id, Collider{glm::vec2(0, 4), glm::vec2(frame.rect.size.x, frame.rect.size.y/2), CollisionFilterGroups ::Ground});
				}
				else {
					sprite_name = "rock";
				}
				pWorld->AddComponent<Sprite>(entity_id, std::move(Sprite{ quad->VAO, atlas, sprite_name, atlas->getSpriteFrame(sprite_name) }));
				pWorld->AddComponent<Position>(entity_id, Position{ pos.x, pos.y });
			}
		}
	}
}
