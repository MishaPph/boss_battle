#include "Network.h"

#include <stdio.h>
#include <iostream>

#include "enet/enet.h"

void Network::init(INetworkListener* listener)
{
	IsServer = false;
	server = nullptr;
	client = nullptr;
	pListener = listener;

	if (enet_initialize() != 0)
	{
		std::cout << "An error occurred while initializing ENet." << "\n";
	}
}

int Network::connect(const bool server, const std::function<void(int)>& connected) {

	on_client_connected = connected;
	if (server)
		return createServer();
	return createClient();
}

bool Network::createServer() {
	ENetAddress address = { 0 };

	address.host = ENET_HOST_ANY; /* Bind the server to the default localhost.     */
	address.port = 1234; /* Bind the server to port 7777. */

	/* create a server */
	server = enet_host_create(&address, 32, 2, 0, 0);

	if (server == nullptr)
	{
		std::cout << "An error occurred while trying to create an ENet server host." << "\n";
		return false;
	}

	const auto hostName = new char[25];
	if (enet_address_get_host_ip(&address, hostName, 25) == 0) {
		std::cout << hostName << "\n";
	}
	else {
		std::cout << "fail to get host name" << "\n";
	}
	delete[] hostName;
	state = ConnectionState::Connected;
	std::cout << "Started a server... " << address.host << "\n";
	IsServer = true;
	if(pListener)
		pListener->serverStarted();
	return true;
}

bool Network::createClient() {
	client = { 0 };
	client = enet_host_create(nullptr /* create a client host */,
		1 /* only allow 1 outgoing connection */,
		2 /* allow up 2 channels to be used, 0 and 1 */,
		0 /* assume any amount of incoming bandwidth */,
		0 /* assume any amount of outgoing bandwidth */);
	if (client == nullptr) {
		fprintf(stderr, "An error occurred while trying to create an ENet client host.\n");
		return false;
	}

	ENetAddress address = { 0 };
	
	clientPeer = { 0 };

	enet_address_set_host(&address, "localhost");
	
	std::cout << "createClient " << address.host << "\n";

	address.port = 1234;
	/* Initiate the connection, allocating the two channels 0 and 1. */
	clientPeer = enet_host_connect(client, &address, 2, 0);
	if (clientPeer == nullptr) {
		fprintf(stderr, "No available peers for initiating an ENet connection.\n");
		return false;
	}
	const auto hostName = new char[25];
	if (enet_address_get_host_ip(&address, hostName, 25) == 0) {
		std::cout << hostName << "\n";
	}
	else {
		std::cout << "fail to get host name" << "\n";
	}
	client_id = 0;
	std::cout << "connecting to server... " << "\n";

	state = ConnectionState::Connecting;
	return true;
}

void Network::disconnect() {
	if (!isClient())
		return;

	ENetEvent event = {  };

	auto disconnected = false;
	enet_peer_disconnect(clientPeer, 0);
	while (enet_host_service(client, &event, 3000) > 0) {
		switch (event.type) {
		case ENET_EVENT_TYPE_RECEIVE:
			enet_packet_destroy(event.packet);
			break;
		case ENET_EVENT_TYPE_DISCONNECT:
			puts("Disconnection succeeded.");
			disconnected = true;
			break;
		default: ;
		}
	}

	// Drop connection, since disconnection didn't successed
	if (!disconnected) {
		enet_peer_reset(clientPeer);
		clientPeer = nullptr;
	}
	state = ConnectionState::Connected;
	enet_deinitialize();
}

void Network::clear() {

	if (state != ConnectionState::Connected && clientPeer) {
		enet_peer_reset(clientPeer);
	}
	state = ConnectionState::None;

	if(server != nullptr)
		enet_host_destroy(server);

	if (client != nullptr)
		enet_host_destroy(client);

	enet_deinitialize();
}

void Network::updateClient() {
	if (state == ConnectionState::Connecting && waitingTimeToConnectToServer == 3000) {
		std::cout << " fail to reach server " << "\n";
		createServer();
		return;
	}
	waitingTimeToConnectToServer++;

	ENetEvent event = {  };
	int code = enet_host_service(client, &event, 0);
	if (code != 0) {
		switch (event.type) {
		case ENET_EVENT_TYPE_CONNECT:
			std::cout << "A client connected " << event.peer->address.host << ", port " << event.peer->address.port << "\n";
			state = ConnectionState::Connected;
			//on_client_connected(event.peer->connectID);
			client_id = event.peer->connectID;
			pListener->clientConnected(client_id);
			break;

		case ENET_EVENT_TYPE_RECEIVE:
			/*std::cout << "A packet was received from "
				<< event.packet->dataLength
				<< event.packet->data
				<< event.peer->data
				<< event.channelID << std::endl;*/
			/* Clean up the packet now that we're done using it. */
			pListener->messageReceived((char *)(event.packet->data), event.packet->dataLength);
			enet_packet_destroy(event.packet);
			break;

		case ENET_EVENT_TYPE_DISCONNECT:
			std::cout << event.peer->data << " disconnected." << "\n";
			pListener->clientDisconnected(client_id);
			/* Reset the peer's client information. */
			event.peer->data = nullptr;
			break;
		default: ;
		}
	}
}

void Network::updateServer() {
	ENetEvent event;
	enet_host_service(server, &event, 0);
	switch (event.type) {
	case ENET_EVENT_TYPE_CONNECT:
		std::cout << "A new client connected from " << event.peer->address.host << event.peer->address.port << "\n";
		/* Store any relevant client information here. */
		event.peer->data = (void *)"Client information";
		sendClientId(event.peer);
		//on_client_connected(event.peer->connectID);
		pListener->clientConnected(event.peer->connectID);
		break;

	case ENET_EVENT_TYPE_RECEIVE:
		/*std::cout << "A packet of length %lu containing %s was received from %s on channel %u."
			<< event.packet->dataLength
			<< event.packet->data
			<< event.peer->data
			<< event.channelID << std::endl;*/
		pListener->messageReceived((char *)(event.packet->data), event.packet->dataLength);
		/* Clean up the packet now that we're done using it. */
		enet_packet_destroy(event.packet);
		break;

	case ENET_EVENT_TYPE_DISCONNECT:
		std::cout << event.peer->data << " disconnected." << "\n";
		/* Reset the peer's client information. */
		pListener->clientDisconnected(event.peer->connectID);
		event.peer->data = nullptr;
		break;
	case ENET_EVENT_TYPE_NONE:
		break;
	}
}

void Network::update() {

	if (isClient()) {
		updateClient();
		return;
	}
	if (state != ConnectionState::Connected)
		return;

	updateServer();
}

void Network::sendClientId(ENetPeer* peer) {
	std::shared_ptr<byte[]> ar(new byte[1]);
	ar[0] = 1;
	const auto packet = enet_packet_create(&ar, 1, ENET_PACKET_FLAG_RELIABLE);
	enet_peer_send(peer, 0, packet);
}

void Network::sendPacketReliable(const void *data, const int size) const
{
	ENetPacket *packet = enet_packet_create(data, size, ENET_PACKET_FLAG_RELIABLE);
	if (isServer()) {
		for (size_t i = 0; i < server->peerCount; i++)
		{
			enet_peer_send(&server->peers[i], 0, packet);
		}
	} else
		enet_peer_send(clientPeer, 0, packet);
}

void Network::sendPacketUnreliable(const void *data, const int size, const int peerId) const
{
	ENetPacket *packet = enet_packet_create(data, size, ENET_PACKET_FLAG_UNRELIABLE_FRAGMENT);
	if (isServer())
		enet_peer_send(&server->peers[peerId], 0, packet);
	else
		enet_peer_send(clientPeer, 0, packet);
}
