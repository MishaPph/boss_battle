﻿#include "Game.h"
#include "beengine/Core.h"
#include "HUDFps.h"
#include "Map.h"
#include "Physics.h"
#include "Factory.h"
#include "GameTime.h"
#include "Network.h"

#include <chrono>
#include <sstream>
#include <iomanip>

#include "ecs/World.h"

#include "Systems/RenderSystem.h"
#include "Systems/PlayerInputSystem.h"
#include "Systems/PlayerSystem.h"
#include "Systems/AnimationSystem.h"
#include "Systems/MotionSystem.h"
#include "Systems/AnimatorSystem.h"
#include "Systems/WeaponSystem.h"
#include "Systems/DrawColliderSystem.h"
#include "Systems/CollisionSystem.h"
#include "Systems/BlinkSystem.h"
#include "Systems/EnemySystem.h"
#include "Systems/BulletSystem.h"
#include "Systems/NetworkSystem.h"

using namespace beengine;
#define SKIP_MENU 1

Game::Game(): maxFrameTime(0), accumulator(0), currentTime(0), simulationFrame(0), showColliders(false)
{
	gameMode = GameMode::Server;
}

Game::~Game()
{
}

void Game::init() 
{
	startTime = std::chrono::high_resolution_clock::now();
	maxFrameTime = 0.25f;
}

void Game::run()
{
	pNetwork = std::make_shared<Network>();
	window = std::make_shared<Window>();
	window->init("Boss Battle", 1280, 800, false);
#if !SKIP_MENU
	drawMenu();
#endif
	std::cout << "Create server " << gameMode << "\n";

	entityMngr = std::make_shared<ecs::EntityManager>();
	pWorld = std::make_shared<ecs::World>(entityMngr);
	pCamera = std::make_shared<Camera>(window);
	
	const auto resources = std::make_shared<Resources>("../Resources/");
	const auto factory = std::make_shared<Factory>(resources);
	
	Map map(resources);
	map.load("map1.bmp");
	map.setup(pWorld);

	Physics::registerGroup(CollisionFilterGroups::Player, CollisionFilterGroups::Ground);

	createSystems(factory);
	
	if (gameMode == GameMode::Server)
	{
		createNetworkSystems(gameMode == GameMode::Server, factory);
	}

	pWorld->init();

	factory->CreateBoss(0, pWorld);
	if (pNetwork->IsServer) {
		factory->CreateHero(NetworkSystem::newId(0), 0, true, pWorld);
	}
	
	drawWorld();
}

void Game::close() const
{
	pNetwork->clear();
	window->close();
}

bool Game::IsServerKeyDown() const
{
	return glfwGetKey(window->GetGLWindow(), GLFW_KEY_W) == GLFW_PRESS ||
				glfwGetKey(window->GetGLWindow(), GLFW_KEY_UP) == GLFW_PRESS;
}

bool Game::IsOfflineKeyDown() const
{
	return glfwGetKey(window->GetGLWindow(), GLFW_KEY_S) == GLFW_PRESS ||
				glfwGetKey(window->GetGLWindow(), GLFW_KEY_DOWN) == GLFW_PRESS;
}

void Game::drawMenu()
{
	const std::string start   = "Start ";
	const std::string connect = "Connect";

	//world
	while (!glfwWindowShouldClose(window->GetGLWindow()))
	{
		if (glfwGetKey(window->GetGLWindow(), GLFW_KEY_ESCAPE) == GLFW_PRESS) {
			glfwSetWindowShouldClose(window->GetGLWindow(), true);
		}
		
		if (gameMode == GameMode::Offline && IsServerKeyDown()) {
			gameMode = GameMode::Server;
		}
		if (gameMode == GameMode::Server && IsOfflineKeyDown()) {
			gameMode = GameMode::Offline;
		}

		if (glfwGetKey(window->GetGLWindow(), GLFW_KEY_ENTER) == GLFW_PRESS) {
			return;
		}

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		const bool isServer = gameMode == GameMode::Server;
		Debug::GetInstance()->Text((isServer ? "+" : "-") + start, 300, 320, 1.0f + isServer * 0.1f, glm::vec3(1, 0.5f*(1 - isServer), 1));
		Debug::GetInstance()->Text((!isServer ? "+" : "-") + connect, 300, 220, 1.0f + (1 - isServer) * 0.1f, glm::vec3(1, 0.5f*(isServer), 1));

		Debug::GetInstance()->Text("Press 'Enter' to start", 300, 50, 0.4f, glm::vec3(0.8f, 0.8f, 0.8f));

		window->swapBuffer();
		glfwPollEvents();
	}
}

void Game::drawWorld() {

	HUDFps fpsCounter;
	//world
	while (!glfwWindowShouldClose(window->GetGLWindow()))
	{
		if (glfwGetKey(window->GetGLWindow(), GLFW_KEY_ESCAPE) == GLFW_PRESS) {
			glfwSetWindowShouldClose(window->GetGLWindow(), true);
		}

		const auto newTime = timeSinceStart();
		auto frameTime = newTime - currentTime;

		if (frameTime > maxFrameTime)
			frameTime = maxFrameTime;

		currentTime = newTime;

		accumulator += frameTime;

		while (accumulator >= GameTime::fixedDeltaTime)
		{
			fixedUpdate();
			accumulator -= GameTime::fixedDeltaTime;
		}

		const auto alpha = accumulator / GameTime::fixedDeltaTime;

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		pNetwork->update();

		updateSystems(frameTime, alpha);

		fpsCounter.update(frameTime);

		std::stringstream stream;
		stream << std::fixed << std::setprecision(2) << fpsCounter.fps;
		std::string s = stream.str();

		Debug::GetInstance()->Text(s, 1, 1, 0.3f, glm::vec3(1, 1, 1));

		window->swapBuffer();
		glfwPollEvents();
	}
}

void Game::createSystems(const std::shared_ptr<Factory>& factory)
{
	pWorld->registerSystem<RenderSystem>(std::make_shared<RenderSystem>(pCamera));
	pWorld->registerSystem<PlayerInputSystem>(std::make_shared<PlayerInputSystem>(window->GetGLWindow()));
	pWorld->createAndRegisterSystem<PlayerSystem>();
	pWorld->createAndRegisterSystem<AnimationSystem>();
	pWorld->createAndRegisterSystem<MotionSystem>();
	pWorld->createAndRegisterSystem<AnimatorSystem>();
	pWorld->createAndRegisterSystem<WeaponSystem>(factory);
	pWorld->registerSystem<DrawColliderSystem>(std::make_shared<DrawColliderSystem>(pCamera));
	pWorld->registerSystem<CollisionSystem>(std::make_shared<CollisionSystem>(Rect(glm::vec2(-300.0f, -200.0f), glm::vec2(600, 400))));
	pWorld->createAndRegisterSystem<BlinkSystem>();
	pWorld->createAndRegisterSystem<EnemySystem>();
	pWorld->createAndRegisterSystem<BulletSystem>();
}

void Game::createNetworkSystems(bool server, const std::shared_ptr<Factory>& factory)
{
	pWorld->registerSystem<NetworkSystem>(std::make_shared<NetworkSystem>(server, pNetwork, factory));
}

double Game::timeSinceStart() const
{
	const auto duration = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - startTime);
	return duration.count() / 1000000.0;
}

void Game::updateSystems(float dt, float alpha)
{
	pWorld->getSystem<PlayerInputSystem>()->update(dt, alpha);
	pWorld->getSystem<PlayerSystem>()->update(dt, alpha);
	pWorld->getSystem<WeaponSystem>()->update(dt, alpha);
	pWorld->getSystem<AnimationSystem>()->update(dt, alpha);
	pWorld->getSystem<AnimatorSystem>()->update(dt, alpha);
	pWorld->getSystem<MotionSystem>()->update(dt, alpha);
	pWorld->getSystem<RenderSystem>()->update(dt, alpha);

	if(showColliders)
		pWorld->getSystem<DrawColliderSystem>()->update(dt, alpha);

	pWorld->getSystem<BlinkSystem>()->update(dt, alpha);

	glfwSetInputMode(window->GetGLWindow(), GLFW_STICKY_KEYS, GL_FALSE);

	if (glfwGetKey(window->GetGLWindow(), GLFW_KEY_F2) == GLFW_PRESS) {
		showColliders = !showColliders;
	}
}

void Game::fixedUpdate()
{
	pWorld->getSystem<PlayerSystem>()->fixedUpdate();

	if (pNetwork->IsServer) {
		pWorld->getSystem<EnemySystem>()->fixedUpdate();
	}
	pWorld->getSystem<MotionSystem>()->fixedUpdate();
	pWorld->getSystem<CollisionSystem>()->fixedUpdate();
	pWorld->getSystem<BulletSystem>()->fixedUpdate();
	if (pNetwork->state != ConnectionState::None)
	{
		pWorld->getSystem<NetworkSystem>()->fixedUpdate();
	}
	simulationFrame++;
}
