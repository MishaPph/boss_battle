#include "ecs/System.h"

using namespace ecs;

// Add a reference to the parent world
void System::registerWorld(std::shared_ptr<World> world) {
	parentWorld = world;
}

void System::unRegisterWorld(std::shared_ptr<World>  world) {
	parentWorld = nullptr;
}
