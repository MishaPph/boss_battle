#include "ecs/World.h"
#include "ecs/System.h"
#include "ecs/Event.h"

using namespace ecs;

void World::init() const
{
	for (const auto& it : systems) {
		it.second->init();
	}
}

World::World(const std::shared_ptr<EntityManager>& mngr) {
	pEntitymngr = mngr;
	pEventBus = std::make_shared<EventBus>();
}

World::~World()
{
	for (const auto component_manager : componentManagers)
	{
		delete component_manager;
	}
}
