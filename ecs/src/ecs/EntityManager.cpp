#include "ecs\EntityManager.h"
#include <iostream>

using namespace ecs;

EntityID EntityManager::NewEntity()
{
	if (!freeEntities.empty())
	{
		EntityIndex newIndex = freeEntities.back();
		freeEntities.pop_back();
		EntityID newID = CreateEntityId(newIndex, GetEntityVersion(entities[newIndex].id));
		entities[newIndex].id = newID;
		return entities[newIndex].id;
	}
	entities.push_back({ CreateEntityId(EntityIndex(entities.size()), 0), ComponentMask() });
	return entities.back().id;
}

void EntityManager::DestroyEntity(EntityID id)
{
	EntityID newID = CreateEntityId(EntityIndex(-1), GetEntityVersion(id) + 1);
	entities[GetEntityIndex(id)].id = newID;
	entities[GetEntityIndex(id)].mask.reset();
	freeEntities.push_back(GetEntityIndex(id));
}

bool EntityManager::Has(EntityIndex index, char componentId)
{
	if (!entities[index].mask.test(componentId))
		return false;
	return true;
}

