#pragma once

#include <vector>

#ifndef ECS_SCENEVIEW_H
#define ECS_SCENEVIEW_H

namespace ecs {

	template<typename... ComponentTypes>
	class SceneView
	{
	private:
		std::shared_ptr<EntityManager> pEntityMngr { nullptr };
		ComponentMask componentMask;
		bool all { false };
	public:
		explicit SceneView(const std::shared_ptr<EntityManager>& pEntityMngr) : pEntityMngr(pEntityMngr)
		{
			if (sizeof...(ComponentTypes) == 0)
			{
				all = true;
			}
			else
			{
				// Unpack the template parameters into an initializer list
				int componentIds[] = { 0, GetId<ComponentTypes>() ... };
				for (int i = 1; i < (sizeof...(ComponentTypes) + 1); i++)
					componentMask.set(componentIds[i]);
			}
		}

		struct EntityIterator
		{
		private:
			EntityIndex index;
			std::shared_ptr<EntityManager> pEntityMngr;
			ComponentMask mask;
			bool all { false };
		public:
			EntityIterator(): index(0), pEntityMngr(nullptr)
			{
			}
			EntityIterator(const std::shared_ptr<EntityManager>& pEntityMngr, const EntityIndex index, const ComponentMask mask, const bool all)
				: index(index), pEntityMngr(pEntityMngr), mask(mask), all(all) {}

			EntityID operator*() const
			{
				return pEntityMngr->entities[index].id;
			}

			bool operator==(const EntityIterator& other) const
			{
				return index == other.index || index == pEntityMngr->entities.size();
			}

			bool operator!=(const EntityIterator& other) const
			{
				return index != other.index && index != pEntityMngr->entities.size();
			}

			bool validIndex() const
			{
				return
					// It's a valid entity ID
					pEntityMngr->IsEntityValid(pEntityMngr->entities[index].id) &&
					// It has the correct component mask
					(all || mask == (mask & pEntityMngr->entities[index].mask));
			}

			EntityIterator& operator++()
			{
				do
				{
					index++;
				} while (index < pEntityMngr->entities.size() && !validIndex());
				return *this;
			}
		};

		EntityIterator begin() const
		{
			unsigned int firstIndex = 0;
			const size_t size = pEntityMngr->entities.size();
			while (firstIndex < size &&
				(componentMask != (componentMask & pEntityMngr->entities[firstIndex].mask)
					|| !pEntityMngr->IsEntityValid(pEntityMngr->entities[firstIndex].id)))
			{
				firstIndex++;
			}
			return EntityIterator(pEntityMngr, firstIndex, componentMask, all);
		}

		EntityIterator end() const
		{
			return EntityIterator(pEntityMngr, static_cast<EntityIndex>(pEntityMngr->entities.size()), componentMask, all);
		}
	};
}

#endif