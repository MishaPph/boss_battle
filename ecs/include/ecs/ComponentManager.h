#pragma once

#ifndef ECS_COMPONENTMANAGER_H
#define ECS_COMPONENTMANAGER_H

namespace ecs {
	constexpr int MAX_ENTITIES = 1000;
	struct ComponentManager
	{
	public:
		ComponentManager(const size_t elementsize)
		{
			// We'll allocate enough memory to hold MAX_ENTITIES, each with element size
			elementSize = elementsize;
			pData = new char[elementSize * ecs::MAX_ENTITIES];
		}

		~ComponentManager()
		{
			delete[] pData;
		}

		void* get(const size_t index) const
		{
			// looking up the component at the desired index
			return pData + index * elementSize;
		}

		template<typename T>
		T * Allocate(const size_t index) {
			return new (get(index)) T();
		}
		template<typename T>
		T * Allocate(const size_t index, const T &&data) {
			return new (get(index)) T(data);
		}
		size_t ElementSize() const
		{
			return elementSize;
		}
	private:
		char* pData{ nullptr };
		size_t elementSize { 0 };
	};
}

#endif