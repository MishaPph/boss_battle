#include <vector>
#include <bitset>
#include <iostream>
#include "Component.h"

#ifndef ECS_ENTITYMANAGER_H
#define ECS_ENTITYMANAGER_H

#define INVALID_ENTITY CreateEntityId(EntityIndex(-1), 0)

namespace ecs {

	// Some typedefs to aid in reading
	typedef unsigned long long EntityID;
	typedef unsigned int EntityIndex;
	typedef unsigned int EntityVersion;
	const int MAX_COMPONENTS = 32;

	typedef std::bitset<MAX_COMPONENTS> ComponentMask;

	class EntityManager {
	private:
		std::vector<EntityIndex> freeEntities;
		struct EntityDesc
		{
			EntityID id;
			ComponentMask mask;
		};
	public:
		std::vector<EntityDesc> entities;
		EntityID NewEntity();
		void DestroyEntity(EntityID id);

		template<typename T>
		bool Has(const EntityIndex index) const
		{
			const auto size =  entities.size();
			if(index >= size) 
			{
				std::cout << "Out of range " << index << ", current size = " << size << std::endl;
				return false;
			};
			const int componentId = ecs::GetId<T>();
			if (!entities[index].mask.test(componentId))
				return false;
			return true;
		}

		bool Has(EntityIndex index, char componentId);

		ComponentMask GetComponentMask(EntityIndex index) {
			return entities[index].mask;
		}

		template<typename T>
		void Remove(EntityID id)
		{
			auto index = GetEntityIndex(id);
			// ensures you're not accessing an entity that has been deleted
			if (entities[index].id != id)
				return;

			int componentId = ecs::GetId<T>();
			entities[index].mask.reset(componentId);
		}

		void RegisterComponent(EntityIndex index, int componentId) {
			// Set the bit for this component to true and return the created component
			entities[index].mask.set(componentId);
		}

		void UnRegisterComponent(EntityIndex index, int componentId) {
			// Set the bit for this component to true and return the created component
			entities[index].mask.flip(componentId);
		}

		inline bool IsEntityValid(EntityID id)
		{
			// Check if the index is our invalid index
			return (id >> 32) != EntityIndex(-1);
		}

		inline EntityID CreateEntityId(EntityIndex index, EntityVersion version)
		{
			auto result = ((EntityID)index << 32) | ((EntityID)version);
			// Shift the index up 32, and put the version in the bottom
			return ((EntityID)index << 32) | ((EntityID)version);
		}

		EntityIndex GetEntityIndex(EntityID id)
		{
			// Shift down 32 so we lose the version and get our index
			return id >> 32;
		}

		EntityVersion GetEntityVersion(EntityID id)
		{
			// Cast to a 32 bit int to get our version number (loosing the top 32 bits)
			return static_cast<EntityVersion>(id);
		}
	};
};

#endif