#pragma once
#ifndef ECS_EVENT_H
#define ECS_EVENT_H

#include <list>
#include <map>
#include <typeinfo>
#include <typeindex>
#include <memory>

namespace ecs {
	struct Event {};

	class HandlerFunctionBase {};

	template<class EventType>
	class CallEventFunctionHandler : public HandlerFunctionBase
	{
	public:
		virtual void call(const EventType& event) = 0;
	};

	template<class T, class EventType>
	class MemberFunctionHandler : public CallEventFunctionHandler<EventType>
	{
	public:
		typedef void (T::*MemberFunction)(const EventType&);

		MemberFunctionHandler(T * instance, MemberFunction fn) : instance{ instance }, memberFunction{ fn } {}

		void call(const EventType& event) override
		{
			(instance->*memberFunction)(event);
		}

	private:
		T * instance;
		MemberFunction memberFunction;
	};

	typedef std::list<HandlerFunctionBase*> HandlerList;

	class EventBus {
	public:
		template<typename EventType>
		void publish(const EventType&& evnt) {
			auto handlers = subscribers[typeid(EventType)];
			if (handlers == nullptr) {
				return;
			}

			for (auto & handler : *handlers) {
				if (handler != nullptr) {
					auto he = static_cast<CallEventFunctionHandler<EventType>*>(handler);
					if (he != nullptr) {
						he->call(evnt);
					}
				}
			}
		}

		template<class T, class EventType>
		void subscribe(T * instance, void (T::*fn)(const EventType&)) {
			auto handlers = subscribers[typeid(EventType)];

			if (handlers == nullptr) {
				handlers = std::make_shared<HandlerList>();
				subscribers[typeid(EventType)] = handlers;
			}
			auto r = new MemberFunctionHandler<T, EventType>(instance, fn);
			handlers->push_back(r);
		}
	private:
		std::map<std::type_index, std::shared_ptr<HandlerList>> subscribers;
	};
};

#endif