#pragma once
#include <bitset>
#include <vector>
#include <map>
#include <iostream>
#include <cassert>

#include "Component.h"
#include "ComponentManager.h"
#include "EntityManager.h"

#ifndef ECS_WORLD_H
#define ECS_WORLD_H

namespace ecs {

	class System;
	class EventBus;

	class World : public std::enable_shared_from_this<World>
	{
		std::vector<ComponentManager*> componentManagers;
		std::map<int, std::shared_ptr<System>> systems;
		std::shared_ptr<EntityManager> pEntitymngr;
		std::shared_ptr<EventBus> pEventBus;
	public:
		
		std::shared_ptr<EntityManager> GetEntityManager() const
		{
			return pEntitymngr;
		}
		
		std::shared_ptr<EventBus> GetEventBus() const
		{
			return pEventBus;
		}

		explicit World(const std::shared_ptr<EntityManager>& mngr);
		~World();
		
		template<typename T>
	    void AddComponentManagersIfNeed() {
			int componentId = ecs::GetId<T>();

			if (componentManagers.size() <= componentId) // Not enough component pool
			{
				componentManagers.resize(componentId + 1);
			}
			if (componentManagers[componentId] == nullptr) // New component, make a new pool
			{
				componentManagers[componentId] = new ComponentManager(sizeof(T));
			}
		}

		template<typename T>
		T* Assign(const EntityID id) {
			const int componentId = ecs::GetId<T>();
			const auto index = pEntitymngr->GetEntityIndex(id);
			AddComponentManagersIfNeed<T>();

			auto component = componentManagers[componentId]->Allocate<T>(index);
			pEntitymngr->RegisterComponent(index, componentId);

			return component;
		}

		template<typename T>
		void AddComponent(const EntityID id, const T &&data) {
			const int componentId = ecs::GetId<T>();
			const auto index = pEntitymngr->GetEntityIndex(id);
			AddComponentManagersIfNeed<T>();
			// Looks up the component in the pool, and initializes it with placement new
			auto component = componentManagers[componentId]->Allocate<T>(index, std::move(data));

			pEntitymngr->RegisterComponent(index, componentId);
		}

		template<typename T>
		void RemoveComponent(const EntityID id) {
			const int componentId = ecs::GetId<T>();
			const auto index = pEntitymngr->GetEntityIndex(id);

			pEntitymngr->UnRegisterComponent(index, componentId);
		}

		template<typename T>
		T* Get(const EntityID id)
		{
			const auto index = pEntitymngr->GetEntityIndex(id);
			if (!pEntitymngr->Has<T>(index))
				return nullptr;
			const int componentId = ecs::GetId<T>();
			T* pComponent = static_cast<T*>(componentManagers[componentId]->get(index));
			return pComponent;
		}

		template<typename T>
        T& GetRef(const EntityID id)
		{
			const auto index = pEntitymngr->GetEntityIndex(id);
			const int componentId = ecs::GetId<T>();
			T* pComponent = static_cast<T*>(componentManagers[componentId]->get(index));
			return *pComponent;
		}
		
		size_t GetComponentSize(const int componentId) const
		{
			return componentManagers[componentId]->ElementSize();
		}

		void* GetComponent(const int componentId, const EntityID id) const
		{
			const auto index = pEntitymngr->GetEntityIndex(id);
			return componentManagers[componentId]->get(index);
		}

		template<typename T>
		bool Has(const EntityID id) const
		{
			return pEntitymngr->Has<T>(pEntitymngr->GetEntityIndex(id));
		}

		template <class T> std::shared_ptr<T> createAndRegisterSystem()
		{
			return registerSystem(std::make_shared<T>());
		}
		
		template <class T, class... Types>
		std::shared_ptr<T> createAndRegisterSystem(Types&&... args)
		{
			return registerSystem(std::make_shared<T>(std::forward<Types>(args)...));
		}

		template <class T>
		std::shared_ptr<T> registerSystem(std::shared_ptr<T> system)
		{
			int id = ecs::GetSystemId<T>();
			systems.insert({ id, system });
			system->registerWorld(shared_from_this());
			return system;
		}

		template <class T>
		std::shared_ptr<T> getSystem()
		{
			const int id = ecs::GetSystemId<T>();
			return std::static_pointer_cast<T>(systems[id]);
		}

		template <class T>
		void unregisterSystem() {
			const int id = ecs::GetSystemId<T>();
			const auto it = systems.find(id);
			it->second->unRegisterWorld(shared_from_this());
			systems.erase(it, systems.end());
		}

		void init() const;
	};
}

#endif