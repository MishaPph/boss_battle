#pragma once
#ifndef ECS_COMPONENT_H
#define ECS_COMPONENT_H

namespace ecs {

	extern int s_componentCounter;
	template <class T>
	int GetId() noexcept
	{
		static int s_componentId = s_componentCounter++;
		return s_componentId;
	}

	extern int s_systemCounter;
	template <class T>
	int GetSystemId() noexcept
	{
		static int s_systemId = s_systemCounter++;
		return s_systemId;
	}
}
#endif