#pragma once

#ifndef ECS_SYSTEM_H
#define ECS_SYSTEM_H

#include "ecs/EntityManager.h"
#include "ecs/World.h"

namespace ecs {

	class System {
	public:
		// Initialize the System — This happens *before* the game starts but *after* the world has been registered.
		virtual void init() {}

		// Called every game update
		virtual void update(const float dt, float alpha) {}

		virtual void fixedUpdate() {}
		// Called every game render
		virtual void render() {}

		// Add a reference to the parent world
		void registerWorld(std::shared_ptr<World> world);
		void unRegisterWorld(std::shared_ptr<World>  world);

	protected:
		~System() = default;
		// Reference to our parent world
		std::shared_ptr<World> parentWorld;

		template<typename T>
		T* Get(const EntityID id)
		{
			return parentWorld->Get<T>(id);
		}

		template<typename T>
		T& GetRef(const EntityID id) const
		{
			return parentWorld->GetRef<T>(id);
		}
	};
}

#endif