#pragma once
#ifndef TEXTURE2D_H_
#define TEXTURE2D_H_

#include <string>

namespace beengine {
	class Texture2D
	{
	public:
		static unsigned int loadTexture(const char *path, bool gamacorrection = false);
		static unsigned int create(unsigned int width, unsigned int height);
		static void release(unsigned int textureId);
		static unsigned char* loadRaw(const std::string& path, int* width, int* height);
		static void unloadRaw(void * st);
	};
}
#endif