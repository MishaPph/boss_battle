
#ifndef QUAD_H
#define QUAD_H

namespace beengine {
	class Quad {
	public:
		Quad();
		~Quad();

		unsigned int VAO, VBO;

		static float vertices[];
	};
}

#endif