#ifndef RECT_H
#define RECT_H

#include <glm/glm.hpp>

namespace beengine {
	
	struct Rect {
		glm::vec2 position;
		glm::vec2 size;

		Rect(): position(), size()
		{
		}

		Rect(const glm::vec2 p, const glm::vec2 z): position(p), size(z)
		{
		}

		bool intersects(const Rect& rect) const noexcept
		{
			if (rect.position.x < position.x + size.x 
				&& position.x < rect.position.x + rect.size.x 
				&& rect.position.y < position.y + size.y)
			{
				return position.y < rect.position.y + rect.size.y;
			}
			
			return false;
		}
		
		friend std::ostream& operator<<(std::ostream& os, const Rect& rect)
		{
			os << "p:(" << rect.position.x << ',' << rect.position.y << ')'<< "s:(" << rect.size.x << ',' << rect.size.y << ')';
			return os;
		}
	};
}

#endif