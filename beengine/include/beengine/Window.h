#pragma once

#ifndef BEENGINE_WINDOW_H_
#define BEENGINE_WINDOW_H_

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <functional>

namespace beengine {

	class Window {
	public:
		Window();
		
		int init(const char* title, int screenWidth, int screenHeight, bool fullScreen);
		int getScreenWidth() const { return m_screenWidth; }
		int getScreenHeight() const { return m_screenHeight; }
		void swapBuffer() const;
		static void close();

		GLFWwindow* GetGLWindow() const noexcept  {
			return window;
		}

		using EventSizeCallback = std::function<void(int, int)>;

		void SetEventResizeCallback(const EventSizeCallback& callback) { m_Data.EventResizeCallback = callback; }
		const GLFWvidmode* GetMonitorResolution() const;
	private:
		int m_screenWidth;
		int m_screenHeight;
		GLFWwindow* window;
		GLFWmonitor* glfw_monitor;
		
		struct WindowData
		{
			EventSizeCallback EventResizeCallback;
		};

		WindowData m_Data;
	};
}

#endif