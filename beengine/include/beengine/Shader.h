#pragma once
#ifndef SHADER_H
#define SHADER_H

#include <glad/glad.h> // include glad to get all the required OpenGL headers
#include <glm/glm.hpp>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

namespace beengine {
	namespace SharedConst
	{
		const std::string Folder = "../Resources/Shaders/";
	}
	
	class Shader
	{
	public:
		// the program ID
		unsigned int ID;

		// constructor reads and builds the shader
		Shader(const std::string &vertexPath, const std::string &fragmentPath, const std::string &geometryPath);
		Shader(const std::string &vertexPath, const std::string &fragmentPath);
		Shader(const std::string &shaderName);
		static unsigned int compile(const std::string &, unsigned int GLenum);

		// use/activate the shader
		void use() const;
		// utility uniform functions
		void setBool(const std::string &name, bool value) const;
		void setInt(const std::string &name, int value) const;
		void setFloat(const std::string &name, float value) const;

		void setMat4(const std::string &name, const glm::mat4 model) const;
		void setMat4(const std::string & name, const GLfloat * matrix) const;

		void setVec4(const std::string &name, glm::vec4 vect) const;
		void setVec3(const std::string &name, float x, float y, float z) const;
		void setVec3(const std::string &name, glm::vec3 vect) const;

		void setVec2(const std::string &name, glm::vec2 vect) const;
	};
}
#endif