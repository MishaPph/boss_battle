#pragma once
#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>
#include <memory>

namespace beengine {

	class Window;
	
	class CameraConst
	{
	public:
		static float ScaleCoef;
	};

	class Camera {
	protected:
		float aspect;
		float size;
		void resize(int w, int h);
	public:
		
		glm::mat4 matrix;
		explicit Camera(std::shared_ptr<Window> window);
	};
}
#endif