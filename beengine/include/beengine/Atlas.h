#ifndef ATLAS_H
#define ATLAS_H

#include <string>
#include <vector>
#include <map>

#include "json.hpp"

#include "beengine/Rect.h"

#include <glm/glm.hpp>

using json = nlohmann::json;

namespace beengine {

	struct SpriteFrame {
		Rect rect;
		glm::vec2 pivot;
	};
	
	class Atlas {
		std::map<std::string, SpriteFrame> frames;
		std::map<std::string, std::vector<std::string>> animations;
		std::string texture_name;
		std::vector<std::string> empty_animation_array;
	public:

		~Atlas();
		
		int width, height;
		unsigned int textureId;

		const std::string& getTextureName() {
			return texture_name;
		}

		SpriteFrame& getSpriteFrame(const std::string& sprite_name) {
			return frames[sprite_name];
		}

		const std::vector<std::string>& getAnimation(const std::string& animation_name);

		void registerAnimation(const std::string& animation_name, std::vector<std::string> animation_frames) {
			animations.insert({ animation_name, animation_frames });
		}

		void attachTexture(const unsigned int texture) {
			textureId = texture;
		}

		void loadData(const std::string& path);
	};
}

#endif