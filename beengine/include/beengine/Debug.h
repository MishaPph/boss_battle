#ifndef DEBUG_H
#define DEBUG_H

#include <string>

#include "beengine/Font.h"
#include "beengine/Quad.h"
#include "beengine/Shader.h"

namespace beengine {
	class Debug {
		class DebugData {
			Shader* shader;
			Font* font;
		public:
			unsigned int VAO;
			unsigned int VBO;

			DebugData(const std::string& shaderName) {
				shader = new Shader(shaderName);
				font = new Font();
			}

			~DebugData()
			{
				delete shader;
				delete font;
			}

			Shader& GetShader() {
				return *shader;
			}
			Font& GetFont() {
				return *font;
			}
		};

		static std::unique_ptr<Debug> instance_;

	protected:

		Debug() {}

		std::shared_ptr<DebugData> data;

		void initData();
	public:

		static std::unique_ptr<beengine::Debug>& GetInstance();

		Debug(Debug &other) = delete;

		void operator=(const Debug &) = delete;

		void Text(const std::string& text, float x, float y, float size, glm::vec3 color);
	};
}

#endif