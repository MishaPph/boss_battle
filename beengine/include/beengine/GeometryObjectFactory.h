#pragma once
#include "Quad.h"

namespace beengine
{
    class GeometryObjectFactory
    {
        static Quad* _quad;
    public:
        static const Quad& GetQuad();
    };
}
