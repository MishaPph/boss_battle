#pragma once

#ifndef RESOURCES_H_
#define RESOURCES_H_

#include <string>
#include <map>
#include <memory>

namespace beengine {

	class Atlas;
		
	class File
	{
	public:
		static std::string loadFile(const std::string& filePath);
	};
	
	class Resources {
	private:
		std::string GetTextureFolder() const noexcept
		{
			return root_path + "Textures/";
		}

		std::string GetAtlasesFolder() const noexcept
		{
			return root_path + "Atlases/";
		}

		std::map<std::string, unsigned int> _textures;
		std::map<std::string, std::shared_ptr<Atlas>> _atlases;
		std::string root_path;
	public:
		Resources(const std::string& path)
		{
			root_path = path;
		}
		Resources(Resources &other) = delete;
		void operator=(const Resources &) = delete;
		Resources(Resources &&other) = delete;
		void operator=(const Resources &&) = delete;
		
		std::string loadTextFile(const std::string& asset_path) const;
		
		unsigned int loadTexture2D(const std::string& asset_path);

		std::shared_ptr<Atlas> loadAtlas(const std::string& asset_path);
		void unLoadAtlas(const std::string& asset_path);
		void unLoadAtlas(const std::shared_ptr<Atlas>& atlas);

		unsigned char* loadRawImage(const std::string& asset_path, int* width, int* height) const;
		void unloadRawImage(void* data);
		
	};
}
#endif
