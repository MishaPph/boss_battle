#include "beengine/Debug.h"

std::unique_ptr<beengine::Debug> beengine::Debug::instance_ = nullptr;

void beengine::Debug::initData()
{
	data = std::make_shared<DebugData>("text");

	//TODO::add to event
	glm::mat4 projection = glm::ortho(0.0f, static_cast<float>(800), 0.0f, static_cast<float>(600));
	data->GetShader().use();
	data->GetShader().setMat4("projection", projection);
	data->GetFont().load("../Resources/Fonts/arial.ttf");

	//create buffer
	glGenVertexArrays(1, &data->VAO);
	glGenBuffers(1, &data->VBO);
	glBindVertexArray(data->VAO);
	glBindBuffer(GL_ARRAY_BUFFER, data->VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

std::unique_ptr<beengine::Debug>& beengine::Debug::GetInstance()
{
	if (instance_ == nullptr) {
		instance_ = std::unique_ptr<Debug>(new Debug());
	}
	return instance_;
}

void beengine::Debug::Text(const std::string& text, float x, float y, float size, glm::vec3 color) {
	if (data == nullptr) {
		initData();
	}

	// activate corresponding render state	
	data->GetShader().use();

	glUniform3f(glGetUniformLocation(data->GetShader().ID, "textColor"), color.x, color.y, color.z);
	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(data->VAO);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// iterate through all characters
	std::string::const_iterator c;
	for (c = text.begin(); c != text.end(); c++)
	{
		FontCharacter ch = data->GetFont().Characters[*c];

		float xpos = x + ch.Bearing.x * size;
		float ypos = y - (ch.Size.y - ch.Bearing.y) * size;

		float w = ch.Size.x * size;
		float h = ch.Size.y * size;
		// update VBO for each character
		float vertices[6][4] = {
			{ xpos,     ypos + h,   0.0f, 0.0f },
			{ xpos,     ypos,       0.0f, 1.0f },
			{ xpos + w, ypos,       1.0f, 1.0f },

			{ xpos,     ypos + h,   0.0f, 0.0f },
			{ xpos + w, ypos,       1.0f, 1.0f },
			{ xpos + w, ypos + h,   1.0f, 0.0f }
		};
		// render glyph texture over quad
		glBindTexture(GL_TEXTURE_2D, ch.TextureID);
		// update content of VBO memory
		glBindBuffer(GL_ARRAY_BUFFER, data->VBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices); // be sure to use glBufferSubData and not glBufferData

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		// render quad
		glDrawArrays(GL_TRIANGLES, 0, 6);

		// now advance cursors for next glyph (note that advance is number of 1/64 pixels)
		x += (ch.Advance >> 6) * size; // bitshift by 6 to get value in pixels (2^6 = 64 (divide amount of 1/64th pixels by 64 to get amount of pixels))
	}
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
}