#include "beengine/Camera.h"
#include "beengine/Window.h"

#include <glm/ext/matrix_clip_space.hpp>

namespace beengine
{
	float CameraConst::ScaleCoef = 4.2f;

	Camera::Camera(std::shared_ptr<Window> window)
	{
		window->SetEventResizeCallback([this](const int x, const int y) { this->resize(x, y); });
		resize(window->getScreenWidth(), window->getScreenHeight());
	}

	void Camera::resize(const int w, const int h) {
		size = h / CameraConst::ScaleCoef;
		aspect = (float)h / w;
		matrix = glm::ortho(-size / aspect, size / aspect, -size, size, -100.f, 100.f);
	}
}