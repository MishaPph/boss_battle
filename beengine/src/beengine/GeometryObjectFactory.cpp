#include "beengine/GeometryObjectFactory.h"

#include "beengine/Quad.h"

using namespace beengine;

Quad* GeometryObjectFactory::_quad = nullptr;

const Quad& GeometryObjectFactory::GetQuad()
{
   if (_quad == nullptr)
        _quad = new Quad();
    return *_quad;
}
