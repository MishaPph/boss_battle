#include "beengine/Resources.h"
#include "beengine/Texture2D.h"
#include "beengine/Atlas.h"

#include <iostream>
#include <fstream>
#include <sstream>

using namespace beengine;

std::string File::loadFile(const std::string& filePath)
{
	std::ifstream file;
	// ensure ifstream objects can throw exceptions:
	file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	try
	{
		file.open(filePath.c_str());
		if (file.fail())
		{
			std::cout << "ERROR::Resources::FILE_NOT_FOUND " << filePath << '\n';
			return "fail";
		}
		std::stringstream stream;
		// read file's buffer contents into streams
		stream << file.rdbuf();
		file.close();
		return stream.str();
	}
	catch (std::ifstream::failure& e)
	{
		std::cout << "ERROR::Resources::FILE_NOT_SUCCESFULLY_READ " << filePath << ", exception code: " << e.code() << '\n';
	}

	return "";
}

std::string Resources::loadTextFile(const std::string& fileName) const
{
	const auto fullPath = root_path + fileName;
	return File::loadFile(fullPath);
}

unsigned int Resources::loadTexture2D(const std::string& path)
{
	const auto result = _textures.find(path);
	if (result != _textures.end())
		return result->second;

	const auto fullPath = GetTextureFolder() + path;
	const auto texturePtr = Texture2D::loadTexture(fullPath.c_str());
	_textures[path] = texturePtr;
	return texturePtr;
}

std::shared_ptr<Atlas> Resources::loadAtlas(const std::string& path)
{
	const auto result = _atlases.find(path);

	if (result != _atlases.end())
		return result->second;
	
	const auto fullPath = GetAtlasesFolder() + path;
	const auto atlas = std::make_shared<Atlas>();
	atlas->loadData(fullPath +".json");
	const auto textureId = loadTexture2D(atlas->getTextureName());
	atlas->attachTexture(textureId);
	_atlases[path] = atlas;
	return atlas;
}

void Resources::unLoadAtlas(const std::string& path)
{
	const auto result = _atlases.find(path);
	if (result == _atlases.end())
		return;
	
	Texture2D::release(result->second->textureId);
	result->second.reset();
	
	_atlases.erase(result);
}

void Resources::unLoadAtlas(const std::shared_ptr<Atlas>& atlas)
{
	unLoadAtlas(atlas->getTextureName());
}

unsigned char* Resources::loadRawImage(const std::string& path, int* width, int* height) const
{
	const auto fullPath = root_path + path;
	return Texture2D::loadRaw(fullPath, width, height);
}

void Resources::unloadRawImage(void* data)
{
	Texture2D::unloadRaw(data);
}