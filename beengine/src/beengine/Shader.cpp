#include "beengine/Shader.h"
#include <glm/glm.hpp>
#include "beengine/Resources.h"

using namespace beengine;

Shader::Shader(const std::string &vertexPath, const std::string &fragmentPath, const std::string &geometryPath)
{
	auto vShaderCode = File::loadFile(SharedConst::Folder + vertexPath + ".vs");
	auto fShaderCode = File::loadFile(SharedConst::Folder + fragmentPath + ".fs");
	auto gShaderCode = File::loadFile(SharedConst::Folder + geometryPath + ".gs");

	// 2. compile shaders
	int success;

	unsigned int vertex = compile(vShaderCode, GL_VERTEX_SHADER);
	unsigned int fragment = compile(fShaderCode, GL_FRAGMENT_SHADER);
	unsigned int geometry = compile(gShaderCode, GL_GEOMETRY_SHADER);

	// shader Program
	ID = glCreateProgram();
	glAttachShader(ID, vertex);
	glAttachShader(ID, fragment);
	glAttachShader(ID, geometry);
	glLinkProgram(ID);
	// print linking errors if any
	glGetProgramiv(ID, GL_LINK_STATUS, &success);
	if (!success)
	{
		char infoLog[512];
		glGetProgramInfoLog(ID, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
	}
	// delete the shaders as they're linked into our program now and no longer necessery
	glDeleteShader(vertex);
	glDeleteShader(fragment);
	glDeleteShader(geometry);
}

Shader::Shader(const std::string &vertexPath, const std::string &fragmentPath)
{
	const auto vShaderCode = File::loadFile(SharedConst::Folder + vertexPath + ".vs");
	const auto fShaderCode = File::loadFile(SharedConst::Folder + fragmentPath + ".fs");

	// 2. compile shaders
	int success;

	unsigned int vertex = compile(vShaderCode, GL_VERTEX_SHADER);
	unsigned int fragment = compile(fShaderCode, GL_FRAGMENT_SHADER);

	// shader Program
	ID = glCreateProgram();
	glAttachShader(ID, vertex);
	glAttachShader(ID, fragment);
	glLinkProgram(ID);
	// print linking errors if any
	glGetProgramiv(ID, GL_LINK_STATUS, &success);
	if (!success)
	{
		char infoLog[512];
		glGetProgramInfoLog(ID, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
	}
	// delete the shaders as they're linked into our program now and no longer necessery
	glDeleteShader(vertex);
	glDeleteShader(fragment);
}

Shader::Shader(const std::string &shaderName)
{
	const auto vShaderCode = File::loadFile(SharedConst::Folder + shaderName + ".vs");
	const auto fShaderCode = File::loadFile(SharedConst::Folder + shaderName + ".fs");

	int success;

	unsigned int vertex = compile(vShaderCode, GL_VERTEX_SHADER);
	unsigned int fragment = compile(fShaderCode, GL_FRAGMENT_SHADER);

	// shader Program
	ID = glCreateProgram();
	glAttachShader(ID, vertex);
	glAttachShader(ID, fragment);
	glLinkProgram(ID);
	// print linking errors if any
	glGetProgramiv(ID, GL_LINK_STATUS, &success);
	if (!success)
	{
		char infoLog[512];
		glGetProgramInfoLog(ID, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
	}
	// delete the shaders as they're linked into our program now and no longer necessery
	glDeleteShader(vertex);
	glDeleteShader(fragment);
}

unsigned int Shader::compile(const std::string &code, unsigned int GLenum)
{
	const auto fragment = glCreateShader(GLenum);
	auto c_str = code.c_str();
	glShaderSource(fragment, 1, &c_str, nullptr);
	glCompileShader(fragment);

	int success;
	glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		char infoLog[512];
		glGetShaderInfoLog(fragment, 512, nullptr, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << c_str << infoLog << '\n';
	}
	return fragment;
}

void Shader::use() const
{
	glUseProgram(ID);
}

void Shader::setBool(const std::string &name, bool value) const
{
	glUniform1i(glGetUniformLocation(ID, name.c_str()), (int)value);
}

void Shader::setInt(const std::string &name, int value) const
{
	glUniform1i(glGetUniformLocation(ID, name.c_str()), value);
}

void Shader::setFloat(const std::string &name, float value) const
{
	glUniform1f(glGetUniformLocation(ID, name.c_str()), value);
}

void Shader::setMat4(const std::string &name, const glm::mat4 model) const
{
	setMat4(name, &model[0][0]);
}

void Shader::setMat4(const std::string &name, const GLfloat *params) const
{
	glUniformMatrix4fv(glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, params);
}

void Shader::setVec3(const std::string &name, float x, float y, float z) const
{
	glUniform3f(glGetUniformLocation(ID, name.c_str()), x, y, z);
}

void Shader::setVec4(const std::string &name, glm::vec4 vect) const
{
	glUniform4f(glGetUniformLocation(ID, name.c_str()), vect.x, vect.y, vect.z, vect.w);
}

void Shader::setVec3(const std::string &name, glm::vec3 vect) const
{
	glUniform3f(glGetUniformLocation(ID, name.c_str()), vect.x, vect.y, vect.z);
}

void Shader::setVec2(const std::string & name, glm::vec2 vect) const
{
	glUniform2f(glGetUniformLocation(ID, name.c_str()), vect.x, vect.y);
}