#include "beengine/Atlas.h"
#include <fstream>
#include <iostream>

beengine::Atlas::~Atlas()
{
	frames.clear();
	animations.clear();
}

const std::vector<std::string>& beengine::Atlas::getAnimation(const std::string& animation_name) {
	if (animations.find(animation_name) == animations.end()) {
		return empty_animation_array;
	}
	return animations[animation_name];
}

void beengine::Atlas::loadData(const std::string& path) {
	std::ifstream input(path);
	if (input.fail()) {
		std::cout << "file isn't open:" << path << "\n";
		return;
	}
	json json_file;
	input >> json_file;

	for (auto& el : json_file["frames"].items()) {
		auto name = el.key();
		auto frame = el.value()["frame"];
		
		SpriteFrame spriteFrame;
		spriteFrame.rect.position.x = frame["x"];
		spriteFrame.rect.position.y = frame["y"];
		spriteFrame.rect.size.x = frame["w"];
		spriteFrame.rect.size.y = frame["h"];

		if (el.value().contains("anchor")) {
			auto anchor = el.value()["anchor"];
			spriteFrame.pivot.x = anchor["x"];
			spriteFrame.pivot.y = anchor["y"];
		}
		else {
			spriteFrame.pivot.x = 0.5f;
			spriteFrame.pivot.y = 0.5f;
		}

		frames.insert({ name, spriteFrame });
	}

	for (auto& el : json_file["animations"].items()) {
		std::vector<std::string> vect = el.value();
		std::string key = el.key();
		animations.insert({ key, vect });
	}

	auto size = json_file["meta"]["size"];
	width = size["w"];
	height = size["h"];
	texture_name = json_file["meta"]["image"];
}