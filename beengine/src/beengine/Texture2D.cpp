#include "beengine\Texture2D.h"

#include <glad\glad.h>
#include <iostream>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

using namespace beengine;

unsigned int Texture2D::create(unsigned int width, unsigned int height)
{
	unsigned int texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	return texture;
}

void Texture2D::release(const unsigned int textureId)
{
	glDeleteTextures(1, &textureId);
}

unsigned char * Texture2D::loadRaw(const std::string& path, int * width, int * height)
{
	int nrComponents;
	stbi_set_flip_vertically_on_load(true);
	return stbi_load(path.c_str(), width, height, &nrComponents, 0);
}

void Texture2D::unloadRaw(void* st)
{
	stbi_image_free(st);
}

unsigned int Texture2D::loadTexture(const char *path, bool gammaCorrection) {
	unsigned int textureID = 0;
	int width, height, nrComponents = 0;
	stbi_set_flip_vertically_on_load(true);
	unsigned char *data = stbi_load(path, &width, &height, &nrComponents, 0);

	if (data)
	{
		glGenTextures(1, &textureID);
		GLenum format;
		GLenum internalformat;
		if (nrComponents == 1) {
			format = GL_RED;
			internalformat = GL_RED;
		}
		else if (nrComponents == 3) {
			format = GL_RGB;
			internalformat = gammaCorrection ? GL_SRGB : GL_RGB;
		}
		else if (nrComponents == 4) {
			format = GL_RGBA;
			internalformat = gammaCorrection ? GL_SRGB_ALPHA : GL_RGBA;
		}
		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, internalformat, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		//glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		stbi_image_free(data);
	}
	else
	{
		std::cout << "Texture failed to load at path: " << path << '\n';
		stbi_image_free(data);
	}
	return textureID;
}