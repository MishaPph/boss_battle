#include <iostream>

#include <beengine/Window.h>

beengine::Window::Window(): m_screenWidth(0), m_screenHeight(0), window(nullptr)
{
	// glfw: initialize and configure
	// ------------------------------
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glfw_monitor = glfwGetPrimaryMonitor();

#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
}

const GLFWvidmode* beengine::Window::GetMonitorResolution() const
{
	return glfwGetVideoMode(glfw_monitor);
}

int beengine::Window::init(const char* title, const int screenWidth, const int screenHeight, const bool fullScreen)
{
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;
	
	// glfw window creation
	// --------------------
	window = glfwCreateWindow(screenWidth, screenHeight, title, fullScreen ? glfw_monitor : nullptr, nullptr);
	if (window == nullptr)
	{
		std::cout << "Failed to create GLFW window" << '\n';
		glfwTerminate();
		return -1;
	}

	glfwSetWindowUserPointer(window, &m_Data);

	glfwMakeContextCurrent(window);

	// Set GLFW callbacks
	glfwSetFramebufferSizeCallback(window, [](GLFWwindow* window, const int width, const int height)
	{
		glViewport(0, 0, width, height);
		const auto window_data = static_cast<WindowData*>(glfwGetWindowUserPointer(window));
		if (window_data->EventResizeCallback != nullptr)
			window_data->EventResizeCallback(width, height);
	});

	// glad: load all OpenGL function pointers
	// ---------------------------------------
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << "\n";
		return -1;
	}

	return 0;
}

void beengine::Window::swapBuffer() const
{
	glfwSwapBuffers(window);
}

void beengine::Window::close()
{
	glfwTerminate();
}
